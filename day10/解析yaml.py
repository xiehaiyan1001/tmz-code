# -*- coding:utf-8 -*-
# @FileName  :解析yaml.py
# @Time      :2020-10-25 11:13
# @Author    :niuhanyang
import yaml


def load_yaml(file_path):
    with open(file_path, encoding='utf-8') as fr:
        return yaml.load(fr, Loader=yaml.SafeLoader)



if __name__ == "__main__":
    pass
