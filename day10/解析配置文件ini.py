# -*- coding:utf-8 -*-
# @FileName  :解析配置文件.py
# @Time      :2020-10-25 10:36
# @Author    :niuhanyang

import configparser
import os

def parse_ini(node,file_path='config.ini'):
    if not os.path.exists(file_path):
        raise Exception("ini文件不存在")

    with open(file_path, encoding='utf-8') as fr:
        c = configparser.ConfigParser()
        c.read_file(fr)

        if node in c.sections():
            result = dict(c[node])
            return result

        # try:
        #     result = dict(c[node])
        # except Exception as e:
        #     print("查找的不存在")
        # else:
        #     return result




if __name__ == "__main__":
    redis_info = parse_ini('mq')
    print(redis_info)

