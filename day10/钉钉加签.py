# -*- coding:utf-8 -*-
# @FileName  :钉钉加签.py
# @Time      :2020-10-25 11:53
# @Author    :niuhanyang

#1、当前的时间戳、加密的字符串 SECe177d5eec65ba21d1a3b3c3536ecac20b3d96175ffc056eda7b3294b0410fa45
#1、时间戳+'\n'+ 加密的字符串  sha256
#2、base64 加密
#3、urlEncode 编码 quote

import time
import requests
import hashlib
import base64
import hmac
from urllib.parse import quote
url = "https://oapi.dingtalk.com/robot/send?access_token=74f3028e80a522f28733f3dab9f0d1060f77a5749da637c531bd999f3828ec9b"
def create_sign():
    secret = 'SECe177d5eec65ba21d1a3b3c3536ecac20b3d96175ffc056eda7b3294b0410fa45'
    timestamp = int(time.time() * 1000)
    sign_before  = '%s\n%s' % (timestamp,secret)
    hsha265 = hmac.new(secret.encode(),sign_before.encode(),hashlib.sha256)
    sign_sha256 = hsha265.digest()
    sign_b64 = base64.b64encode(sign_sha256)
    sign = quote(sign_b64)
    return {"timestamp":timestamp,"sign":sign}

def send_msg_dingding(msg="大家好，我是萌萌哒机器人"):
    data = {
    "msgtype": "text",
    "text": {
        "content": msg
    },
    "at": {
        "atMobiles": [
            "15620922243",
            "13011823376",
            "13240224523",
            "15901399089"
        ],
        "isAtAll": False
    }
    }
    sign = create_sign()
    r = requests.post(url,params=sign,json=data) #?
    print(r.json())

if __name__ == "__main__":
    send_msg_dingding("大家都辛苦了，马上就去吃饭，python")
