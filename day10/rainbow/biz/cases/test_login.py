# -*- coding:utf-8 -*-
# @FileName  :test_login.py
# @Time      :2020-10-25 17:47
# @Author    :niuhanyang
import unittest
from biz.flow.user import UserRequest
from common.utlis import get_redis,get_mysql
from biz.cases.base_case import BaseCase

class TestLogin(BaseCase):
    '''登录接口测试用例'''

    data_file_name = 'login_data.yaml' #

    @classmethod
    def setUpClass(cls):
        cls.redis = get_redis()
        cls.mysql = get_mysql()


    def test_normal(self):
        '''正常登录'''
        username = self.file_data.get('username')
        password = self.file_data.get('password')

        ret = UserRequest.login(username,password)
        self.assertEqual(0,ret.error_code,'返回的错误码不是0')
        self.assertIsNotNone(ret.login_info.login_time,msg='logintime为空')
        redis_key = 'session:%s' % username
        sessionid = self.redis.get(redis_key)
        sql = 'select id from app_myuser where username = "%s";' % username
        db_result = self.mysql.fetchone(sql)
        user_id = db_result.get('id')
        self.assertEqual(sessionid,ret.login_info.sign,msg="返回的session和redis中的不一致")
        self.assertEqual(user_id,ret.login_info.userId,msg="返回的userId和数据库中的不一致")




if __name__ == "__main__":
    pass
