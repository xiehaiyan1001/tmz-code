# -*- coding:utf-8 -*-
# @FileName  :base_case.py
# @Time      :2020-10-25 18:10
# @Author    :niuhanyang
import unittest
from common.config_parse import load_yaml

class BaseCase(unittest.TestCase):
    data_file_name = None

    @property
    def file_data(self):
        data = load_yaml(self.data_file_name)
        return data

    @classmethod
    def get_token(cls,username):
        pass



if __name__ == "__main__":
    pass
