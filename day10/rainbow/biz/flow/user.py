# -*- coding:utf-8 -*-
# @FileName  :user.py
# @Time      :2020-10-25 17:28
# @Author    :niuhanyang
from common.http_request import Request
from biz.support.urls import ServerUrl

class UserRequest:

    @classmethod
    def login(cls,username,password):
        '''
        调用登录接口的
        :param username: 用户名
        :param password: 密码
        :return:
        '''
        data = {
            'username':username,
            'passwd':password
        }
        req = Request(ServerUrl.login_url,data=data)
        return req.post()

    @classmethod
    def register(cls,username,pwd,cpwd):
        '''
        注册
        :param username: 用户名
        :param password: 密码
        :param cpassword: 确认密码
        :return:
        '''
        data = {
            "username":username,
            "pwd":pwd,
            "cpwd":cpwd,
        }
        req = Request(ServerUrl.login_url,data=data)
        return req.post()




if __name__ == "__main__":
    result = UserRequest.login('niuhanyang','aA123456')
    print(result)
