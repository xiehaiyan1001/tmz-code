# -*- coding:utf-8 -*-
# @FileName  :run.py
# @Time      :2020-10-25 15:09
# @Author    :niuhanyang
import unittest
import os,time,sys

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.index(0,BASE_PATH)


from conf.settings import CASE_PATH,REPORT_PATH,dd_template,mail_template
from common import send_msg
from common.HTMLTestRunner import HTMLTestRunner

def run():
    test_suite = unittest.defaultTestLoader.discover(CASE_PATH,'test*.py')
    file_name = 'report_%s.html' % time.strftime('%Y%m%d%H%M%S')
    file_abs = os.path.join(REPORT_PATH,file_name )
    with open(file_abs,'wb') as fw:
        runner = HTMLTestRunner(stream=fw,title='测试报告标题',description='描述')
        case_result = runner.run(test_suite)
        all_count = case_result.failure_count + case_result.success_count
        dd_msg = dd_template % (all_count,case_result.success_count,case_result.failure_count)
        mail_msg = mail_template % (all_count,case_result.success_count,case_result.failure_count)
        send_msg.send_dingding(dd_msg)
        subject = '天马座自动化测试报告-%s' % time.strftime('%Y-%m-%d %H:%M:%S')
        send_msg.send_mail(subject,mail_msg,file_abs)


if __name__ == "__main__":
    run()
