# -*- coding:utf-8 -*-
# @FileName  :test.py
# @Time      :2020-10-25 15:44
# @Author    :niuhanyang

#魔法方法，在一些特定情况下会自动执行这个方法
#访问某个不存在的属性，他会调用__getattr__方法


class NbDict(dict):

    def __getattr__(self, item):
        value = self[item]
        if type(value) == dict:
            value = NbDict(value)  # 如果是字典类型，直接返回DictToObject这个类的对象

        elif isinstance(value, list) or isinstance(value, tuple):
            # 如果是list，循环list判断里面的元素，如果里面的元素是字典，那么就把字典转成DictToObject的对象
            value = list(value)
            for index, obj in enumerate(value):
                if isinstance(obj, dict):
                    value[index] = NbDict(obj)
        return value

if __name__ == "__main__":
    d = {"name":"xiaohei","age":18,'money':666}
    new_d = MyDict(d)
    print(new_d.name)


