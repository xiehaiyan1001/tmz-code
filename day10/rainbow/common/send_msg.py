# -*- coding:utf-8 -*-
# @FileName  :send_msg.py
# @Time      :2020-10-25 16:58
# @Author    :niuhanyang
import yamail
from common.http_request import Request
from common.utlis import parse_ini,create_sign
ddconfig = parse_ini('dingding') #取钉钉的配置信息
mail_config = parse_ini('mail') #取邮箱的配置信息
url = ddconfig.get('url') #钉钉的url
access_token = ddconfig.get('access_token') #access_token
at = ddconfig.get('at','').split(',') #钉钉发送消息的时候ai给谁


def send_dingding(msg):
    data = {
        "msgtype": "text",
        "text": {
            "content": msg
        },
        "at": {
            "atMobiles":at,
            "isAtAll": False
        }
    }
    sign = create_sign()
    sign['access_token'] = access_token
    r = Request(url,params=sign,json=data)
    r.post()

def send_mail(subject,contents,attachments=None):

    smtp = yamail.SMTP(
        host=mail_config.get("host"),  # 改成自己邮箱的邮箱的服务器即可
        user=mail_config.get("user"),
        password=mail_config.get("password")  # 如果是163、qq等免费邮箱的话需要授权码，
        # 自己公司的邮箱，一般都使用密码
    )
    smtp.send(to=mail_config.get("to",'').split(','),  # 发送给谁
              subject=subject,  # 邮件主题
              cc=mail_config.get("cc",'').split(','),  # 抄送，如果是多个人写list
              contents=contents,  # 邮件正文
              attachments=attachments  # 附件，如果是多个附件，写list
              )
    smtp.close()


if __name__ == "__main__":
    send_dingding("哈哈哈，nhy")
