# -*- coding:utf-8 -*-
# @FileName  :用例前置条件.py
# @Time      :2020-10-25 10:05
# @Author    :niuhanyang

import unittest



class Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        #所有用例运行之前，它会执行一次
        print('setUpClass\n')

    @classmethod
    def tearDownClass(cls):
        #所有用例运行完之后，它会执行一次
        print("tearDownClass\n")

    def tearDown(self):
        #每条测试用例运行之后都会执行它
        print("tearDown")

    def setUp(self):
        #每条测试用例运行之前都会先执行它
        print("setUp")


    def testa1(self):
        print('testa1\n')

    def testz(self):
        print("testz\n")

    def testb(self):
        print("testb\n")

    def testa(self):
        print("testa\n")


class OpenAcc(unittest.TestCase):


    def test_open_acc_normal(self):
        '''四要素开户'''
        pass

    def test_open_acc_normal1(self):
        '''两要素'''





if __name__ == "__main__":
    unittest.main()
