export function get_user_id() {
  return localStorage.getItem("user_id")
}
export function get_phone() {
  return localStorage.getItem("phone")
}

export function get_email() {
  return localStorage.getItem("email")
}

export function get_username_zh() {
  return localStorage.getItem("name_zh")
}

