import request from '@/utils/request'

export function get_public_data() {
  return request({
    url: '/api/anole/public_data',
    method: 'get'
  })
}

//获取公共参数

export function set_public_data(data) {
  return request({
    url: '/api/anole/public_data',
    method: 'put',
    data
  })
}

//修改公共参数


export function get_category_data(data) {
  return request({
    url: '/api/anole/category',
    method: 'get',
    params: data
  })
}

export function set_category_data(data) {
  return request({
    url: '/api/anole/category',
    method: 'put',
    data
  })
}

export function add_category_data(data) {
  return request({
    url: '/api/anole/category',
    method: 'post',
    data
  })
}

//url?id=1

export function delete_category_data(data) {
  return request({
    url: '/api/anole/category',
    method: 'delete',
    params: data
  })
}

export function get_interface_data(data) {
  return request({
    url: '/api/anole/interface',
    method: 'get',
    params: data
  })
}

export function add_interface_data(data) {
  return request({
    url: '/api/anole/interface',
    method: 'post',
    data
  })
}

export function set_interface_data(data) {
  return request({
    url: '/api/anole/interface',
    method: 'put',
    data
  })
}

export function del_interface_data(data) {
  return request({
    url: '/api/anole/interface',
    method: 'delete',
    params: data
  })
}
