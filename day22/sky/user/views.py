from django.shortcuts import render

# Create your views here.

from common.custom_view import BaseView,PostView
from common.responses import SkyResponse
from . import models
from . import forms
from django.views import View
from common.utils import get_redis,md5
from user.core import logout
from django.conf import settings
import os,time


class RegisterView(BaseView,PostView):
    form_class = forms.UserForm
    model_class = models.User

class LoginView(View):
    def post(self,request):
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            return SkyResponse(token=form.token)
        else:
            return SkyResponse(code=-1,msg=form.format_errors)

class Logout(View):

    def post(self,request):
        logout(request)
        return SkyResponse()

class ChangePwdView(View):
    def post(self,request):
        form = forms.ChangePwdForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data.get("password")
            new_password = form.cleaned_data.get("new_password")
            md5_password = md5(password,settings.SECRET_KEY)
            if md5_password == request.user.password:
                new_md5_password = md5(new_password, settings.SECRET_KEY)
                request.user.password = new_md5_password
                request.user.save()
                logout(request)
                return SkyResponse()
            else:
                return SkyResponse(code=-1, msg="旧密码不正确！")

        else:
            return SkyResponse(code=-1,msg=form.format_errors)


class UploadView(View):
    def post(self,request):
        f = request.FILES.get("file")
        if f:
            file_name = f.name.rstrip('"') #因为它传过来的文件名有引号
            if file_name in os.listdir(settings.UPLOADS_PATH):
                file_name = time.strftime("%Y%m%d%H%M%S") + file_name
            print(f.size) #文件大小，单位是字节  1kb 1024字节  1m 1024kb  1g 1024m
            with open(os.path.join(settings.UPLOADS_PATH,file_name),'wb') as fw:
                for line in f.chunks(): #循环，一行一行写
                    fw.write(line)
            return SkyResponse()
        else:
            return SkyResponse(-1,"文件不能为空")


class UserInfoView(View):
    def get(self,request):
        d = {"roles": ["admin"], "introduction": "我是管理员",
            "avatar": "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
            "name": request.user.username_zh,
             "user_id":request.user.id,
             "email":request.user.email,
             "phone":request.user.phone
             }
        return SkyResponse(data=d)

