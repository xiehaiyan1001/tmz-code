from django.db import models

# Create your models here.

class User(models.Model):
    phone = models.CharField(max_length=11,unique=True,verbose_name="手机号")
    email = models.EmailField(max_length=50,unique=True,verbose_name="邮箱")
    password = models.CharField(max_length=32,verbose_name="密码")
    username_zh = models.CharField(max_length=20,verbose_name="用户名")
    create_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True,verbose_name='修改时间')

    def __str__(self):
        return self.username_zh

    class Meta:
        db_table = "user"
        ordering = ["-update_time"]