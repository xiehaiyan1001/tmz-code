# -*- coding:utf-8 -*-
# @FileName  :process_put_middleware.py
# @Time      :2020-12-27 16:02
# @Author    :niuhanyang
from django.http import QueryDict
from django.middleware.common import MiddlewareMixin
import json
from common.responses import SkyResponse

class PutMiddleWare(MiddlewareMixin):
    def process_request(self,request):
        if request.method.upper() == 'PUT': #如果是put请求
            contenttype = request.META.get('CONTENT_TYPE')  #获取请求参数的类型
            if 'multipart' in contenttype: #如果是带有文件的这种
                put_data, files = request.parse_file_upload(request.META, request)
                request.PUT = put_data
                request._files = files
            else:#
                put_data = QueryDict(request.body)
                request.PUT = put_data

class JsonMiddleware(MiddlewareMixin):
    '''处理入参是json类型，给request对象里面添加request.PUT'''

    @staticmethod
    def process_request(request):
        # 所有请求先走到这，然后再去请求视图
        if request.method.upper() == 'PUT' or request.method.upper() == 'POST':
            contenttype = request.META.get('CONTENT_TYPE')
            if 'json' in contenttype or 'text' in contenttype:
                if request.body:
                    try:
                        data = json.loads(request.body)
                    except:
                        return SkyResponse(400, 'json格式错误！')
                    else:
                        #setattr(request,request.method.upper(),data)#使用反射的方式
                        if request.method.upper() == 'PUT':
                            request.PUT = data
                        else:
                            request.POST = data







if __name__ == "__main__":
    pass
