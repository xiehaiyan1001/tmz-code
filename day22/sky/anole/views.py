from django.shortcuts import render

# Create your views here.


from . import models
from . import forms
from common.custom_view import SkyView,BaseView,GetView,PutView
from common.utils import model_to_dict
from common.responses import SkyResponse

class CategoryView(SkyView):
    search_field = ["name","prefix"]
    model_class = models.Category
    form_class = forms.CategoryForm

    def get(self, request):
        l = []
        interface_query_set = self.model_class.objects.filter(**self.get_filter_dict).filter(self.get_search_obj)
        page_obj, page_data = self.get_page_data(interface_query_set)

        for d in page_data:
            dic = model_to_dict(d, fields=self.show_field, exclude=self.exclude_field)
            dic["create_user_name"] = d.create_user.username_zh
            dic["update_user_name"] = d.update_user.username_zh
            l.append(dic)

        return SkyResponse(data=l, count=page_obj.count)


class InterfaceView(SkyView):
    search_field = ["name","url","response","desc"]
    filter_field = ["category"]
    model_class = models.Interface
    form_class = forms.InterfaceForm

    def get(self, request):
        l = []
        interface_query_set = self.model_class.objects.filter(**self.get_filter_dict).filter(self.get_search_obj)
        page_obj, page_data = self.get_page_data(interface_query_set)

        for d in page_data:
            dic = model_to_dict(d, fields=self.show_field, exclude=self.exclude_field)
            dic["create_user_name"] = d.create_user.username_zh
            dic["update_user_name"] = d.update_user.username_zh
            dic["method_name"] = d.get_method_display()
            dic["category_name"] = d.category.name
            l.append(dic)

        return SkyResponse(data=l, count=page_obj.count)

import time
class PublicDataView(BaseView,GetView,PutView):
    model_class = models.PublicData
    form_class = forms.PublicForm

    def get(self, request):
        time.sleep(2)
        l = []
        interface_query_set = self.model_class.objects.filter(**self.get_filter_dict).filter(self.get_search_obj)
        page_obj, page_data = self.get_page_data(interface_query_set)
        for d in page_data:
            dic = model_to_dict(d, fields=self.show_field, exclude=self.exclude_field)
            dic["create_user_name"] = d.create_user.username_zh
            dic["update_user_name"] = d.update_user.username_zh
            l.append(dic)
        return SkyResponse(data=l, count=page_obj.count)
