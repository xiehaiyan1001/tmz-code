# -*- coding:utf-8 -*-
# @FileName  :interface_handler.py
# @Time      :2021-01-24 14:41
# @Author    :niuhanyang
from django.http.response import HttpResponse
from common.responses import SkyResponse
from proxy.views import proxy_view
import json,re
from common.log import Log
from .utils import exec_py_code
from anole.models import PublicData

class InterfaceHandler:
    def __init__(self,interface,request):
        self.interface  = interface
        self.request = request
        self.custom_field = [] #存的是所有需要替换的字段
        self.custom_field_data = {} #age:1,token:1，这个最终存的是，替换替换的字段名和字段值

    def handler_proxy(self):
        '''处理需要转发的url'''
        if self.interface.proxy_url:
            try:
                result = proxy_view(self.request,self.interface.proxy_url)
            except:
                return SkyResponse(-1, "接口转发出错，请检查配置的转发url")
            else:
                return result
        else:
            return SkyResponse(-1, "接口转发url未配置！")

    def set_response_header(self,response):
        try:
            result = json.loads(self.interface.response_header)
        except:
            Log.warning("接口 %s，设置响应头出错，json格式不正确" % self.interface.name)
        else:
            if type(result) == dict:
                for k, v in result.items():
                    response[k] = v
            else:
                Log.warning("接口 %s，设置响应头出错，json格式不正确" % self.interface.name)

        return response

    def get_custom_field(self):
        '''用来获取所有需要替换的参数'''
        self.custom_field = re.findall(r'\$\{(.*?)\}', self.interface.response)
        for filed in self.custom_field: #放到一个字典里面
            self.custom_field_data[filed] = None  #{"id":None,"age":None}


    def custom_param_set(self):
        '''用来获取接口层面的自定义参数'''
        if self.interface.code:
            return exec_py_code(self.interface.code)
        return {}

    def custom_public_param_set(self):
        '''用来获取公共参数的'''
        if PublicData.objects.all().count():#判断是否有数据
            public_code = PublicData.objects.all().first()
            return exec_py_code(public_code.code)
        else:
            return {}

    def request_param_set(self):
        params = {} #存整理好的参数
        #处理url里面传的参数
        for k,v in self.request.GET.items():
            params[k] = v
        #处理post里面的参数
        if self.request.method.upper() == "POST":
            for k,v in self.request.POST.items():
                params[k] = v

        # 处理put里面的参数
        if self.request.method.upper() == "PUT":
            for k, v in self.request.PUT.items():
                params[k] = v

        #处理入参是json的
        contenttype = self.request.META.get('CONTENT_TYPE')  # 获取请求参数的类型
        if 'multipart' in contenttype:  # 判断是否为form-data这种类型的
            pass
        else:
            try:
                json_data = json.loads(self.request.body)
            except:
                pass
            else:
                params.update(json_data)

        return params

    def set_custom_field_data(self):
        custom_data = self.custom_param_set() #接口层的代码产生的数据
        public_data = self.custom_public_param_set()#公共参数的代码产生的数据
        req_param_data = self.request_param_set() #从请求参数里面获取到的数据

        for field,value in self.custom_field_data.items():
            req_value = req_param_data.get(field)#从请求参数里面获取的数据
            custom_value = custom_data.get(field)#从接口代码层里面获取对应的字段
            public_value = public_data.get(field)#从公共代码层里面获取对应的字段

            if req_value != None:
                self.custom_field_data[field] = req_value
                continue

            if custom_value != None:#如果接口层的代码有数据，那么就更新一下字段的值
                self.custom_field_data[field] = custom_value
                continue

            if public_value != None:
                self.custom_field_data[field] = public_value
                continue



    def replace_response(self):
        '''替换参数'''
        #{"id":1,"age":33,"addr":None}
        for k,v in self.custom_field_data.items():
            if v != None:
                self.interface.response = self.interface.response.replace("${%s}" % k,str(v))



    def handler_response(self):
        self.get_custom_field() #获取所有需要替换的字段
        self.set_custom_field_data() #set字段对应的值
        self.replace_response() #替换
        ret = HttpResponse(self.interface.response)
        self.set_response_header(ret) #设置响应头
        return ret

    def handler(self):
        if not self.interface.status: #判断接口是否完成
            return SkyResponse(-1, "接口未完成！")

        if self.interface.proxy_flag:#开启了接口转发，走接口转发的逻辑
            return self.handler_proxy()

        return self.handler_response() #处理配置的mock结果


if __name__ == "__main__":
    pass
