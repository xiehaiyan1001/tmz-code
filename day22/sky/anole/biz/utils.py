# -*- coding:utf-8 -*-
# @FileName  :util.py
# @Time      :2021-01-24 14:13
# @Author    :niuhanyang


from anole.const import method_choice
from common.log import Log
def method_to_id(method):
    '''获取请求方式对应的method_id'''
    d = dict(method_choice)
    for k,v in d.items():
        if v == method.upper():
            return k

def exec_py_code(code):
    '''用来执行python代码的'''
    try:
        exec(code)
    except:
        Log.warning("代码执行出错，代码是 {}",code)
    Log.debug("locals,{}",locals())
    return locals()



if __name__ == "__main__":
    pass
