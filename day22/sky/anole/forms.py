# -*- coding:utf-8 -*-
# @FileName  :forms.py
# @Time      :2021-04-11 13:40
# @Author    :niuhanyang

from . import models
from django import forms
from common.sky_form import ExtendForm

class CategoryForm(forms.ModelForm,ExtendForm):

    class Meta:
        model = models.Category
        fields = '__all__'


class InterfaceForm(forms.ModelForm, ExtendForm):
    class Meta:
        model = models.Interface
        fields = '__all__'

    def clean(self):
        #校验转发url
        proxy_url = self.data.get("proxy_url")
        proxy_tag = self.data.get("proxy_tag")
        if proxy_tag and not proxy_url:
            self.add_error("proxy_url","转发开启时，转发url不能为空")

        if not self.instance.id:#判断是否为修改，如果是修改的时候，不做唯一校验
            method = self.data.get("method")
            category = self.data.get("category")
            url = self.data.get("url")
            if models.Interface.objects.filter(method=method,category=category,url=url):
                self.add_error("url", "接口已经存在，请检查url/请求方式/归属分类")

class PublicForm(forms.ModelForm, ExtendForm):
    class Meta:
        model = models.PublicData
        fields = '__all__'

if __name__ == "__main__":
    pass
