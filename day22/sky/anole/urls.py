# -*- coding:utf-8 -*-
# @FileName  :urls.py
# @Time      :2021-01-10 13:42
# @Author    :niuhanyang


from django.urls import path
from . import views

urlpatterns = [
    path('category', views.CategoryView.as_view()),
    path('interface', views.InterfaceView.as_view()),
    path('public_data', views.PublicDataView.as_view()),
]


if __name__ == "__main__":
    pass
