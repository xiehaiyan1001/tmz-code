# -*- coding:utf-8 -*-
# @FileName  :utils.py
# @Time      :2020-12-27 17:28
# @Author    :niuhanyang
from itertools import chain
import datetime
import hashlib
import django_redis

def model_to_dict(instance, fields=None, exclude=None):
    """
    Return a dict containing the data in ``instance`` suitable for passing as
    a Form's ``initial`` keyword argument.

    ``fields`` is an optional list of field names. If provided, return only the
    named.

    ``exclude`` is an optional list of field names. If provided, exclude the
    named from the returned dict, even if they are listed in the ``fields``
    argument.
    """
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        value = f.value_from_object(instance)
        if isinstance(value, datetime.datetime):
            value = value.strftime('%Y-%m-%d %H:%M:%S') #格式化好的时间
            # value = int(value.timestamp() )#时间戳
        if isinstance(value, datetime.date):
            value = value.strftime('%Y-%m-%d')
        data[f.name] = value

    return data


def md5(s,salt=""):
    s = str(s)
    s = s+salt
    m = hashlib.md5(s.encode())
    return m.hexdigest()


def get_redis(name="default"):
    return django_redis.get_redis_connection(name)


if __name__ == "__main__":
    pass
