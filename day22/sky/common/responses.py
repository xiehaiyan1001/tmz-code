# -*- coding:utf-8 -*-
# @FileName  :responses.py
# @Time      :2020-12-06 16:18
# @Author    :niuhanyang
from django.http.response import JsonResponse

def SkyResponse(code=0,msg='操作成功',**kwargs):
    data = {'code':code,'msg':msg}
    data.update(**kwargs)
    return JsonResponse(data,json_dumps_params={'ensure_ascii':False} )
