# -*- coding:utf-8 -*-
# @FileName  :log.py
# @Time      :2020-10-25 14:25
# @Author    :niuhanyang

from loguru import logger
import sys
from .const import LOG_LEVEL,LOG_FILE

class Log:
    logger.remove()#清除它的默认设置设置
    fmt = '[{time}][{level}][{file.path}:line:{line}:function_name:{function}] ||msg={message}'
    #level file function module time message
    logger.add(sys.stdout,level=LOG_LEVEL,format=fmt)#咱们本地运行的时候，在控制台打印
    logger.add(LOG_FILE,level=LOG_LEVEL,format=fmt,encoding='utf-8',enqueue=True,rotation='1 day',retention='10 days')#写在日志文件里面

    debug = logger.debug
    info = logger.info
    warning = logger.warning
    error = logger.error


if __name__ == "__main__":
    Log.info("日志测试")
