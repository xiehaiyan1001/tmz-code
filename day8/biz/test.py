import pymysql

pymysql.install_as_MySQLdb()

from django.conf import settings
from django.core.management.commands.inspectdb import Command

settings.configure(
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'jxz',
            'USER': 'jxz',
            'PASSWORD': '123456',
            'HOST': '118.24.3.40',
            'PORT': '3306'
        },
    }

)  # 设置数据库信息，和django的settings中一样的

code = '''
    @classmethod
    def get_table(cls,id,table_number=8):
        table_index = id % table_number #这个是分表规则，这个是按照某个id模8，分8张表
        cls._meta.db_table = '%s_%s' % (cls._meta.db_table,table_index)
        return cls.objects

'''


# 要写到model里面的函数，因为model文件是咱们自己生成的，就是往里面写一个字符串

def create_model(tables, file_name, db):
    with open(file_name, 'w', encoding='utf-8') as fw:
        # 下面这个代码是把django生成model文件的代码复制出来的
        command = Command()
        table_dict = {'table': tables, 'database': db}
        for line in command.handle_inspection(table_dict):
            if 'class Meta' in line:  # 判断如果是class Meta，在它前面加个函数
                line = code + line
            fw.write('%s\n' % line)
    print('生成models完成')


if __name__ == '__main__':
    tables = ['app_myuser']  # 生成哪些表的model类

    file_name = 'models.py'  # 生成的文件名

    db = 'default'  # 使用哪个数据库，就是上面setting里面配置的

    create_model(tables, file_name, db)