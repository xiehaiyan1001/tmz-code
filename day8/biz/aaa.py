import pymysql
pymysql.install_as_MySQLdb()

from django.db.models.sql.query import Query

from django.conf import settings
import django
settings.configure(
    DATABASES={
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'NAME': 'jxz',
                'USER': 'jxz',
                'PASSWORD': '123456',
                'HOST': '118.24.3.40',
                'PORT': '3306'
            },
        },
    INSTALLED_APPS=('biz',)#这个是必须要指定的，models文件在biz目录下，这个install app就写biz

)#设置数据库信息，和django的settings中一样的
django.setup()
from biz import models

table = models.AppMyuser.get_table(23532532521) #传入id，计算分表号码
print(Query(models.AppMyuser))

# # print(models.AppMyuser._meta.db_table) #打印db_table的值，可以看出来表已经是1这张表了
# new_user = table.get(pk=1)
# print(new_user.username)

table = models.AppMyuser.get_table(23532532521122) #传入id，计算分表号码
print(Query(models.AppMyuser))

# # # print(models.AppMyuser._meta.db_table) #打印db_table的值，可以看出来表已经是1这张表了
# new_user = table.get(pk=1)
# print(new_user.username)