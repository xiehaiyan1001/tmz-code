# -*- coding:utf-8 -*-
# @FileName  :run.py
# @Time      :2020-10-25 15:09
# @Author    :niuhanyang
import datetime
import unittest
import os,time,sys
import threadpool
from threading import Lock

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0,BASE_PATH)


from conf.settings import CASE_PATH,REPORT_PATH,dd_template,mail_template
from common import send_msg,config_parse
from common.HTMLTestRunner import HTMLTestRunner
from common.HTMLTestRunnerNew import HTMLTestRunner as htr

worker_number = int(config_parse.parse_ini('default').get('worker_number'))
lock = Lock()

class RainbowCaseResult:
    failure_count = 0
    success_count = 0
    error_count = 0
    result = [] #存放每条用例运行的结果
    failures = []#存放失败的case



def run(rule='test*.py'):#单线程
    test_suite = unittest.defaultTestLoader.discover(CASE_PATH,rule)
    file_name = 'report_%s.html' % time.strftime('%Y%m%d%H%M%S')
    file_abs = os.path.join(REPORT_PATH,file_name )
    with open(file_abs,'wb') as fw:
        runner = HTMLTestRunner(stream=fw,title='测试报告标题',description='描述')
        case_result = runner.run(test_suite)
        all_count = case_result.failure_count + case_result.success_count
        dd_msg = dd_template % (all_count,case_result.success_count,case_result.failure_count)
        mail_msg = mail_template % (all_count,case_result.success_count,case_result.failure_count)
        send_msg.send_dingding(dd_msg)
        subject = '天马座自动化测试报告-%s' % time.strftime('%Y-%m-%d %H:%M:%S')
        send_msg.send_mail(subject,mail_msg,file_abs)

def case_result_call_back(thread,test_result):
    with lock:
        RainbowCaseResult.failure_count += test_result.failure_count
        RainbowCaseResult.success_count += test_result.success_count
        RainbowCaseResult.error_count += test_result.error_count
        RainbowCaseResult.result += test_result.result
        RainbowCaseResult.failures += test_result.failures



def run_thread(rule='test*.py'):
    test_suite = unittest.defaultTestLoader.discover(CASE_PATH, rule)
    pool = threadpool.ThreadPool(worker_number)

    #1、报告格式不对了 , 拿到所有执行用例的结果，调用generateReport，产生报告
    #2、拿不到结果了 --解决，使用回调函数

    file_name = 'report.html'
    file_name_new = 'report_new.html'
    file_abs = os.path.join(REPORT_PATH, file_name)
    file_abs_new = os.path.join(REPORT_PATH, file_name_new)

    with open(file_abs,'wb') as fw:
        #多线程运行测试用例
        runner = HTMLTestRunner(stream=fw,title='测试报告标题',description='描述')
        reqs = threadpool.makeRequests(runner.run,test_suite,case_result_call_back)
        start_time = datetime.datetime.now() #2020-11-08 14:56:29
        for req  in reqs:
            pool.putRequest(req)
        pool.wait() #等待
        end_time = datetime.datetime.now()

    with open(file_abs,'wb') as fw2,open(file_abs_new,'wb') as fw3:
        #产生不好看的测试报告
        runner2  = HTMLTestRunner(stream=fw2,title='天马座-接口测试报告',description='接口测试报告')
        runner2.startTime = start_time
        runner2.stopTime = end_time
        runner2.generateReport('',RainbowCaseResult)

        #产生好看的测试报告
        runner3  = htr(stream=fw3,title='天马座-接口测试报告',description='接口测试报告')
        runner3.startTime = start_time
        runner3.stopTime = end_time
        runner3.generateReport('',RainbowCaseResult)

    all_count = RainbowCaseResult.failure_count + \
                RainbowCaseResult.success_count + RainbowCaseResult.error_count

    dd_msg = dd_template % (all_count, RainbowCaseResult.success_count,
                            RainbowCaseResult.failure_count,RainbowCaseResult.error_count)
    mail_msg = mail_template % (all_count, RainbowCaseResult.success_count,
                                RainbowCaseResult.failure_count,RainbowCaseResult.error_count
                                )
    send_msg.send_dingding(dd_msg)
    subject = '天马座自动化测试报告-%s' % time.strftime('%Y-%m-%d %H:%M:%S')

    send_msg.send_mail(subject,mail_msg,file_abs)



if __name__ == "__main__":


    if len(sys.argv)>1: #0 1
        case_rule = sys.argv[1] #如何拿到运行python文件的时候传给你的参数??
        run_thread(case_rule)
    else:
        run_thread()

