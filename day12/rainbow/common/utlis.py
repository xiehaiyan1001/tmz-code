# -*- coding:utf-8 -*-
# @FileName  :utlis.py
# @Time      :2020-10-25 14:59
# @Author    :niuhanyang
import redis
import time
import hmac
import base64
import hashlib
from urllib.parse import quote
from common.config_parse import parse_ini
from common.operate_db import MySQL

ddconfig = parse_ini('dingding') #取钉钉的配置信息
secret = ddconfig.get("secret")


mysql_conn_mapper = {} #{mysqlNone:Mysql,mysql2None:Mysql2}
redis_mapper = {}

def get_mysql_old(node='mysql',db=None):
    #问题就是跑着跑着，mysql连不上了
    key = '%s%s'%(node,db) #mysql2None
    if not key in mysql_conn_mapper:
        mysql_info = parse_ini(node)
        if db:
            mysql_info['db'] = db
        mysql = MySQL(**mysql_info)
        mysql_conn_mapper[key] = mysql
    else:
        mysql =  mysql_conn_mapper[key]

    return mysql


def get_mysql(node='mysql',cursor_type='dict',db=None):
    mysql_info = parse_ini(node)
    if db:
        mysql_info['db'] = db
    mysql = MySQL(**mysql_info,cursor_type=cursor_type)

    return mysql


def get_redis(node='redis',db=None):
    key = '%s%s'%(node,db) #mysql2None
    if not key in redis_mapper:
        redis_info = parse_ini(node)
        if db:
            redis_info['db'] = db
        r = redis.Redis(**redis_info,decode_responses=True)
        redis_mapper[key] = r
    else:
        r = redis_mapper[key]
    return r

def create_sign():
    timestamp = int(time.time() * 1000)
    sign_before  = '%s\n%s' % (timestamp,secret)
    hsha265 = hmac.new(secret.encode(),sign_before.encode(),hashlib.sha256)
    sign_sha256 = hsha265.digest()
    sign_b64 = base64.b64encode(sign_sha256)
    sign = quote(sign_b64)
    return {"timestamp":timestamp,"sign":sign}

if __name__ == "__main__":
    c = get_mysql()


