# -*- coding:utf-8 -*-
# @FileName  :custom_class.py
# @Time      :2020-10-25 16:03
# @Author    :niuhanyang

class NbDict(dict):

    def __getattr__(self, item): #{"login_time"}
        value = self.get(item)
        if type(value) == dict:
            value = NbDict(value)  # 如果是字典类型，直接返回DictToObject这个类的对象

        elif isinstance(value, list) or isinstance(value, tuple):
            # 如果是list，循环list判断里面的元素，如果里面的元素是字典，那么就把字典转成DictToObject的对象
            value = list(value)
            for index, obj in enumerate(value):
                if isinstance(obj, dict):
                    value[index] = NbDict(obj)
        return value

if __name__ == "__main__":
    d = {"login_time":1}
    d1 = NbDict(d)
    print(d1.login_time2)