# -*- coding:utf-8 -*-
# @FileName  :operate_db.py
# @Time      :2020-10-25 14:15
# @Author    :niuhanyang
import pymysql
import traceback
from common.log import Log

class MySQL:
    def __init__(self,host,user,password,db,charset='utf8',autocommit=True,port=3306,cursor_type='dict'):
        port  = int(port)
        self.conn = pymysql.connect(user=user,
                                    host=host,password=password,db=db,
                                    port=port,
                                    charset=charset,autocommit=autocommit)
        if cursor_type=='dict':
            self.cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        else:
            self.cursor = self.conn.cursor()
        #{"username":password,}
        Log.info("开始连接mysql")

    def __del__(self):
        self.__close()

    def execute(self,sql):
        try:
            self.cursor.execute(sql)
        except Exception:
            Log.error('sql执行出错，sql语句是{}',sql)
            Log.error(traceback.format_exc())

    def fetchall(self,sql):
        self.execute(sql)
        return self.cursor.fetchall()

    def fetchone(self,sql):
        self.execute(sql)
        return self.cursor.fetchone()


    def __close(self):
        self.cursor.close()
        self.conn.close()





if __name__ == "__main__":
    pass