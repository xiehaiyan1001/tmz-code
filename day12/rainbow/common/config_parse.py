# -*- coding:utf-8 -*-
# @FileName  :config_parse.py
# @Time      :2020-10-25 14:22
# @Author    :niuhanyang
import os
import configparser
import yaml
from common.log import Log
from conf.settings import CONFIG_FILE,CASE_DATA_PATH

def parse_ini(node,file_path=CONFIG_FILE):
    if not os.path.exists(file_path):
        Log.error("配置文件不存在，文件路径{}",file_path)
        raise Exception("ini文件不存在")

    with open(file_path, encoding='utf-8') as fr:
        c = configparser.ConfigParser()
        c.read_file(fr)
        if node in c.sections():
            result = dict(c[node])
            return result
        Log.warning("配置文件中[ {} ]节点不存在",node)

def load_yaml(file_name):
    file_path = os.path.join(CASE_DATA_PATH,file_name)
    if not os.path.exists(file_path):
        Log.error("用例数据文件不存在，文件路径{}",file_path)
        raise Exception("yaml文件不存在")

    with open(file_path, encoding='utf-8') as fr:
        return yaml.load(fr, Loader=yaml.SafeLoader)



if __name__ == "__main__":
    parse_ini("mysql",'mysql.ini')
