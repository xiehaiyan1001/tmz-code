# -*- coding:utf-8 -*-
# @FileName  :params_operate.py
# @Time      :2020-11-08 10:18
# @Author    :niuhanyang
from conf.settings import CASE_DATA_PATH
from common.utlis import get_mysql
import os
import xlrd


class Param:
    @classmethod
    def from_csv(cls,name):
        '''从csv/txt文件中读取参数化内容'''
        abs_path = os.path.join(CASE_DATA_PATH,name)
        data = []
        with open(abs_path,encoding='utf-8') as fr:
            for line in fr:
                line = line.strip()
                if line:
                    data.append(line.split(','))
        return data

    @classmethod
    def from_excel(cls,name):
        data = []
        abs_path = os.path.join(CASE_DATA_PATH,name)
        book = xlrd.open_workbook(abs_path)
        sheet = book.sheet_by_index(0)
        for i in range(1,sheet.nrows):
            data.append(sheet.row_values(i))
        return data

    @classmethod
    def from_mysql(cls,sql,db=None,node='mysql'):
        m = get_mysql(node,db=db,cursor_type=None) #[[a,b,c],[b,cd]]
        return m.fetchall(sql)

    @classmethod
    def from_redis(cls):
        pass

    @classmethod
    def from_mongodb(cls):
        pass






if __name__ == "__main__":
    # result = Param.from_csv('users.csv')
    # result = Param.from_excel('users.xlsx')
    result = Param.from_mysql('select id,username,passwd from app_myuser;')
    print(result)
