# -*- coding:utf-8 -*-
# @FileName  :settings.py
# @Time      :2020-10-25 14:31
# @Author    :niuhanyang

import os

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

LOG_FILE = os.path.join(BASE_PATH,'logs','rainbow.log') #日志文件

REPORT_PATH = os.path.join(BASE_PATH,'report') #报告存放的目录

CASE_PATH = os.path.join(BASE_PATH,'biz','cases') #测试用例的目录

CASE_DATA_PATH = os.path.join(BASE_PATH,'biz','data') #测试数据的目录

CONFIG_FILE = os.path.join(BASE_PATH,'conf','config.ini') #配置文件的目录

LOG_LEVEL = 'INFO' #默认日志级别


jenkins_url = "http://118.24.3.40:8080/job/tmz-auto-case-nhy/HTML_20Report/"


dd_template = '''本次测试共运行%s条用例，通过%s条，失败%s条，错误%s条,具体信息请点击 {} '''.format(jenkins_url)


mail_template = '''
各位好！
    本次测试共运行%s条用例，通过%s条，失败%s条，错误%s条,具体信息请点击 {} '''.format(jenkins_url)


if __name__ == "__main__":
    pass

