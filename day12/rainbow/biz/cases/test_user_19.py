# -*- coding:utf-8 -*-
# @FileName  :test_user.py
# @Time      :2020-11-08 14:10
# @Author    :niuhanyang
from biz.cases.base_case import BaseCase

class TestUser(BaseCase):

    def test_user(self):
        '''测试用户'''
        self.assertEqual(0,0)

if __name__ == "__main__":
    with open('test_user.py',encoding='utf-8') as fr:
        content = fr.read()
        for i in range(1,31):
            name = 'test_user_%s.py' % i
            f = open(name,'w',encoding='utf-8')
            f.write(content)
