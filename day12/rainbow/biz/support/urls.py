# -*- coding:utf-8 -*-
# @FileName  :urls.py
# @Time      :2020-10-25 17:20
# @Author    :niuhanyang

from urllib.parse import urljoin
from common.config_parse import parse_ini


host = parse_ini('server').get('host')

class ServerUrl:
    login_url =  urljoin(host,'/api/user/login') #登录url
    register_url =  urljoin(host,'/api/user/user_reg') #注册url



if __name__ == "__main__":
    pass
