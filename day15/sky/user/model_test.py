# -*- coding:utf-8 -*-
# @FileName  :model_test.py
# @Time      :2020-11-29 14:51
# @Author    :niuhanyang

import os,django

from django.db.models import Q
from django.db import transaction

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sky.settings')
django.setup()
from user import models

#新增
# models.Category.objects.create(name="MySQL")
#
# c = models.Category(name="Oracle")
# c.save()

#外键的时候怎么插入数据
# models.Article.objects.create(
#     title="orm学习",
#     content="orm学习 content",
#     category_id=1
# )#知道id的

# category = models.Category.objects.get(id=2)
# models.Article.objects.create(
#     title="orm学习2",
#     content="orm学习 content",
#     category=category
# )#不知道id的

#修改
#update user set name=lhy ;
#update user set sex="nan" where age > 18;

# c = models.Category.objects.get(id=1)
# c.name = 'JAVA'
# c.save()#修改单条数据

# models.Category.objects.all().update(name="abc")
#修改全表的某个字段

#按照某些条件修改
# models.Category.objects.filter(name="abc").\
#     update(name="ccc",age=13)

#删除
# c = models.Category.objects.get(id=6)
# c.delete()#删除某一条
#models.Category.objects.all().delete()
#删除全表数据
#按照某些条件修改
# models.Category.objects.filter(name="abc").delete()

#复杂的查询
#大于、小于、大于等于、in、not 小于等于、或
# result = models.Article.objects.filter(read_count__gt=0)#大于
# print(result)
# result = models.Article.objects.filter(read_count__gte=0)#大于等于
# print(result)
# result = models.Article.objects.filter(read_count__lt=1)#小于
# print(result)
# result = models.Article.objects.filter(read_count__lte=1)#小于等于
# print(result)
# result = models.Article.objects.exclude(read_count=0) #不等于
# print(result)
# result = models.Article.objects.filter(id__in=[1,2,3])
# print(result)

# result = models.Article.objects.filter(title__contains='ORM') #like，包含某个字符串
# print(result)
# result = models.Article.objects.filter(title__icontains='ORM') #like，忽略大小写包含某个字符串
# print(result)
# result = models.Article.objects.filter(title__endswith='xxx')#结尾
# print(result)

# query_set = models.Article.objects.filter(id=80) # 50
# if query_set:
#     article  = query_set.first()
#     #query_set.last()最后一条
# else:
#     print('这条数据不存在')

#read_count>0 或者 title里面包含 orm


# result = models.Article.objects.filter( Q(read_count__gt=0) | Q(title__contains='orm')   )
# print(result)
# result = models.Article.objects.filter(read_count__isnull=True) #判断是否为null
# print(result)
# models.Article.objects.filter(title='')#是否为空字符串
# print(result)
art = models.Article.objects.get(id=1)
print(art.read_count)
print(art.title)
print(art.category.name)
print(art.category.id)


if __name__ == "__main__":
    pass
