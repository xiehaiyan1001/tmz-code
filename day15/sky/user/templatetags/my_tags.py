# -*- coding:utf-8 -*-
# @FileName  :my_tags.py
# @Time      :2020-11-29 16:57
# @Author    :niuhanyang
from django.template import Library

register = Library()

@register.filter
def mingan(content):
    return content.replace("金三胖",'金正恩')


@register.filter
def mingan2(content,s):
    return content.replace("金三胖",s)

@register.simple_tag
def mingan3(content,*args):
    #你好，金三胖，三胖，sb，SB
    #
    for arg in args:
        content = content.replace(arg,'金正恩')
    return content



if __name__ == "__main__":
    pass
