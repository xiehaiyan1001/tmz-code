from django.db import models

# Create your models here.

#分类表、文章表

class BaseModel(models.Model):
    create_time = models.DateTimeField(verbose_name='创建时间',auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='修改时间',auto_now=True)

    class Meta:
        abstract = True


class Category(BaseModel):
    name = models.CharField(verbose_name='分类名称',max_length=50,unique=True)

    class Meta:
        db_table = 'category'
        verbose_name = '文章分类'
        verbose_name_plural = verbose_name
        ordering = ['-create_time']

    def __str__(self):
        return self.name

class Article(BaseModel):
    title = models.CharField(verbose_name='文章标题',max_length=100,)
    content = models.TextField(verbose_name='文章内容') #长文本类型
    read_count = models.IntegerField(verbose_name='阅读次数',default=0)
    category = models.ForeignKey(Category,on_delete=models.PROTECT,verbose_name='分类')  #外键
    #models.DO_NOTHING #什么也不干
    #models.CASCADE ,会删除
    #models.SET_DEFAULT ，设置一个默认值，1
    #models.SET_NULL #设置成空
    #models.PROTECT#受保护的，如果还有在使用的，不能让你删除
    #models.SET#自定义模式，自己指定

    class Meta:
        db_table = 'article'
        verbose_name = '文章'
        verbose_name_plural = verbose_name
        ordering = ['-create_time']

    def __str__(self):
        return self.title


#makeamrig
#mirget