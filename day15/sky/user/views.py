from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404,HttpResponseRedirect
from django.core.paginator import Paginator
from . import models
# Create your views here.

from hashlib import md5
def sign(s):
    news = 'sdgsdgsgsdgsdx&'+s
    m = md5(news.encode())
    return m.hexdigest()

def nhy(request):
    return render(request,'lhy.html')


#公共的参数

def index(request):
    #获取请求参数
    print("这里是index")
    page_number  = request.GET.get('page',1)
    limit  = request.GET.get('limit',5)
    articles = models.Article.objects.all() #19 / 5 [:5] [5:11]
    page = Paginator(articles,limit) #分页后的对象
    articles = page.page(page_number) #当前页的数据
    #page 1 ,size 5

    print(articles.has_next())#有没有下一页
    # print(articles.next_page_number())#下一页的页码，有下一页的话才有
    print(articles.has_other_pages())#有没有其他页
    print(articles.has_previous())#有没有上一页
    # print(articles.previous_page_number())#上一页的页码
    print(articles.number) #当前页的页码
    print(articles.start_index()) #当前这一页的第一条数据的下标
    print(articles.end_index())#当前这一页的最后一条数据的下标
    #articles.paginator #就是上面分页完之后的对象

    print(page.num_pages) #总共多少页
    print(page.page_range) #分页的范围  1 2 3 4




    return render(request,'index.html',
                  {'articles':articles})

def category(request,id):
    articles = models.Article.objects.filter(category_id=id)
    return render(request,'category.html',{'articles':articles})

def article(request,id):
    art = get_object_or_404(models.Article,id=id)
    return render(request,'detail.html',{'art':art})



def get_sign(request):
    s = request.GET.get('str')
    result = sign(s)
    data = {"code":0,"data":result}
    return JsonResponse(data)


def category_view(request):
    #查询
    # c = models.Category.objects.all() #查询所有的数据
    # for i in c:
    #     print(i.name)
    #     print(i.create_time)
    #     print(i.update_time)
    # print("=======")
    # j = models.Category.objects.get(name="java")#单条数据
    # #必须保证返回的是一条
    # print(j.name)
    #
    # f = models.Category.objects.filter(name='java',id=3)
    # print(f)
    #
    # result = models.Category.objects.filter(name="mysql").exists() #是否存在
    # print(result)
    #
    # result = models.Category.objects.filter(name="mysql").count()
    # #返回多少条
    # print(result)



    c = models.Category.objects.get(id=3)#分类-》文章
    print(c.article_set.count()) #分类下面的所有的文章
    print(c.article_set.all()) #分类下面的所有的文章
    print(c.article_set.filter()) #分类下面的所有的文章


    art = models.Article.objects.get(id=1)#修改
    art.read_count+=1
    art.save()



    return HttpResponse("ok")


def test(request):
    import datetime
    content = "金三胖，sb，SB，三胖，contentcontentcontentcontentcontentcontentcontentcontent"
    title = "tItle"
    stus = ['wxl','lhy','ds','hzy']
    name = "高雯"
    age = 37
    info = {"money":9999}
    cur_date = datetime.datetime.now()
    return render(request,'template_tags.html',locals())


def add_article(request):
    if request.method == 'GET':
        return render(request,'add_article.html')
    title = request.POST.get('title')
    content = request.POST.get('content')
    category_id = request.POST.get('category_id')
    models.Article.objects.create(
        title = title,
        content = content,
        category_id = category_id
    )
    return HttpResponseRedirect('/')