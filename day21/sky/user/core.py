# -*- coding:utf-8 -*-
# @FileName  :core.py
# @Time      :2021-01-10 16:42
# @Author    :niuhanyang
from common.utils import get_redis,md5
from . import const


def logout(request):
    r = get_redis()
    userid_key = const.user_id_prefix + md5(request.user.id)
    token_key = const.token_prefix + request.token
    r.delete(userid_key)
    r.delete(token_key)
