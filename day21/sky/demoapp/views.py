from django.shortcuts import render

# Create your views here.
from common.responses import SkyResponse
from common.utils import model_to_dict
from . import models
from . import forms
from common import custom_view
from django.views import View
from demoapp.test_tools.create_test_data import create_open_account
from proxy.views import proxy_view

class AuthorView(custom_view.SkyView):
    model_class = models.Author
    form_class = forms.AuthorForm
    search_field = ['name']
    filter_field = ['name','id']

class BookView(custom_view.SkyView):
    model_class = models.Book
    form_class = forms.BookForm
    search_field = ['name']
    filter_field = ['name','price','count']

    def get(self, request):
        l = []
        interface_query_set = self.model_class.objects.filter(**self.get_filter_dict).filter(self.get_search_obj)
        page_obj, page_data = self.get_page_data(interface_query_set)
        for instance in page_data:
            dic = model_to_dict(instance, fields=self.show_field, exclude=self.exclude_field)
            dic["author_name"] = instance.author.name
            l.append(dic)

        return SkyResponse(data=l, count=page_obj.count)

class TestAccountView(View):
    # def get(self,request):
    #     count = request.GET.get("count",'')
    #     if count.isdigit():
    #         count = int(count)
    #         data_list = create_open_account(count)
    #         return SkyResponse(data=data_list)
    #     else:
    #         return SkyResponse(-1,"count错误！")

    def get(self,request):
        form = forms.TestAccountForm(request.GET)
        if form.is_valid():
            count = form.cleaned_data.get("count")
            data_list = create_open_account(count)
            return SkyResponse(data=data_list)
        else:
            return SkyResponse(-1,msg=form.format_errors)

class ProxyView(View):
    def get(self,request):
        if request.GET.get("flag") == "1":
            url = "https://www.baifubao.com/callback"
            return proxy_view(request,url)
        else:
            return SkyResponse(msg="走的是本地的接口，没有走代理")