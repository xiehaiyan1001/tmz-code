# -*- coding:utf-8 -*-
# @FileName  :const.py
# @Time      :2020-12-06 17:12
# @Author    :niuhanyang

from django.conf import settings
import os


page_limit = 20 #默认分页数量

url_white_list = [
    'register','login','upload','demo','admin'
] #不需要校验token的url

LOG_LEVEL = "DEBUG" #日志级别
LOG_FILE = os.path.join(settings.BASE_DIR,'logs','sky.log') #日志路径


if __name__ == "__main__":
    pass
