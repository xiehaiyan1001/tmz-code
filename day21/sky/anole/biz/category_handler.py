# -*- coding:utf-8 -*-
# @FileName  :category_handler.py
# @Time      :2021-01-24 14:23
# @Author    :niuhanyang

from anole import models
from . import utils

class CategoryHandler:
    def __init__(self,request):
        self.interface = None
        self.category = None
        self.request = request

    def get_category_prefix(self):
        '''获取分类的url前缀'''
        category_prefix = self.request.path_info.split('/')[1]
        self.category_prefix = category_prefix

    def get_category(self):
        '''获取分类'''
        category = models.Category.objects.filter(prefix=self.category_prefix).first()
        if category:
            self.category = category

    def get_interface_url(self):
        '''获取接口的url'''
        self.interface_url = self.request.path_info.replace("/%s" % self.category_prefix, '')

    def get_method_id(self):
        '''获取请求方式对应的请求方式id'''
        self.method_id = utils.method_to_id(self.request.method)

    def get_interface(self):
        interface = models.Interface.objects.filter(category=self.category, url=self.interface_url,
                                                    method=self.method_id).first()  # get post
        if interface:
            self.interface = interface

    def handler(self):
        self.get_category_prefix()
        self.get_category()
        self.get_interface_url()
        self.get_method_id()
        self.get_interface()




if __name__ == "__main__":
    pass
