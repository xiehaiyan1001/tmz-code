# -*- coding:utf-8 -*-
# @FileName  :middlers.py
# @Time      :2021-01-24 13:51
# @Author    :niuhanyang

from django.middleware.common import MiddlewareMixin
from common.responses import SkyResponse
from .category_handler import CategoryHandler
from .interface_handler import InterfaceHandler


class AnoleMiddler(MiddlewareMixin):

    def process_request(self, request):
        category_handler = CategoryHandler(request)
        category_handler.handler()
        if category_handler.category:  # 判断是否有分类
            if category_handler.interface:  # 判断是否有接口
                interface_handler = InterfaceHandler(category_handler.interface,request)
                return interface_handler.handler()
            else:
                return SkyResponse(-1, "接口不存在，请检查url/请求方式")
