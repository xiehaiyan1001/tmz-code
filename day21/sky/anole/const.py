# -*- coding:utf-8 -*-
# @FileName  :const.py
# @Time      :2021-01-24 10:40
# @Author    :niuhanyang


method_choice = (
    (0,'GET'),
    (1,'POST'),
    (2,'PUT'),
    (3,'DELETE'),
)

if __name__ == "__main__":
    print(dict(method_choice))
