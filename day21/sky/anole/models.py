from django.db import models
from user.models import User
# Create your models here.
from . import const

class BaseModel(models.Model):
    create_time = models.DateTimeField(auto_now_add=True,verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True,verbose_name="修改时间")
    class Meta:
        abstract = True

class PublicData(BaseModel):
    code = models.TextField(default="",verbose_name="代码")
    create_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name="创建用户",related_name="create_user_data")
    update_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name="修改用户",related_name="update_user_data")

    class Meta:
        verbose_name = "公共参数"
        verbose_name_plural = verbose_name


class Category(BaseModel):
    name = models.CharField(verbose_name="分类名称",max_length=50)
    prefix = models.CharField(max_length=50,verbose_name="url前缀",unique=True)
    create_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name="创建用户",related_name="create_user_c")
    update_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name="修改用户",related_name="update_user_c")


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "分类"
        verbose_name_plural = verbose_name


class Interface(BaseModel):
    name = models.CharField(verbose_name="接口名称",max_length=50)
    desc = models.CharField(max_length=100,default="",null=True,blank=True,verbose_name="接口描述")
    url = models.CharField(verbose_name="接口url",max_length=200)
    method = models.SmallIntegerField(choices=const.method_choice,verbose_name="请求方式")
    response = models.TextField(default="{}",verbose_name="响应报文",blank=True,null=True)
    response_header = models.TextField(default="{}",verbose_name="响应头",blank=True,null=True)
    code = models.TextField(default="",verbose_name="自定义代码",blank=True,null=True)
    category = models.ForeignKey(Category,on_delete=models.DO_NOTHING,verbose_name="分类")
    proxy_url = models.CharField(max_length=200,blank=True,null=True,verbose_name="转发的url")
    proxy_flag = models.BooleanField(verbose_name="是否开启代理",default=False)
    status = models.BooleanField(verbose_name="是否完成",default=True)
    create_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name="创建用户",related_name="create_user_interface")
    update_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,verbose_name="修改用户",related_name="update_user_interface")


    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["url","method","category"] #联合唯一
        verbose_name = "接口"
        verbose_name_plural = verbose_name



