# -*- coding:utf-8 -*-
# @FileName  :process_token_middleware.py
# @Time      :2021-01-10 15:35
# @Author    :niuhanyang


from django.middleware.common import MiddlewareMixin
from common import const,utils
from user.const import token_prefix
from common.responses import SkyResponse
import pickle


class TokenMiddleWare(MiddlewareMixin):

    def check_white_list(self,path): #user/register  user/login
        for url in const.url_white_list: #register login
            if url in path:
                return True


    def process_request(self,request):
        if not self.check_white_list(request.path_info):
            token = request.META.get("HTTP_TOKEN")
            if token:
                r = utils.get_redis()
                token_key = token_prefix+token
                if r.exists(token_key):
                    user = pickle.loads(r.get(token_key))
                    request.user = user
                    request.token = token
                else:
                    return SkyResponse(-1, "用户未登录！")
            else:
                return SkyResponse(-1,"没有传token")

