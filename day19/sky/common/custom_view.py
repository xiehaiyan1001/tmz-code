# -*- coding:utf-8 -*-
# @FileName  :custom_view.py
# @Time      :2020-12-27 17:48
# @Author    :niuhanyang


from common.utils import model_to_dict
from django.views import View
from common.responses import SkyResponse
from django.db.models import Q
from django.core.paginator import Paginator
from common.const import page_limit

class BaseView(View):
    filter_field = []
    search_field = []
    show_field = [] #返回的时候，返回哪些字段
    exclude_field = [] #返回的时候不返回哪些字段
    model_class = None
    form_class = None

    @property
    def get_filter_dict(self):
        filter_dict = {}
        for field in self.filter_field:
            value = self.request.GET.get(field)
            if value:
                filter_dict[field] = value
        return filter_dict

    @property
    def get_search_obj(self):
        q_obj = Q()#空白的q对象
        search = self.request.GET.get('search')
        if search:
            for field in self.search_field:
                d = {'%s__contains' % field:search}
                q_obj = q_obj | Q(**d) #这是每次在拼Q对象，生成or条件
        return q_obj

    def get_page_data(self,obj_list):
        try:
            limit = int(self.request.GET.get('limit',page_limit)) #limit = s
            page = int(self.request.GET.get('page',1))
        except:
            limit = page_limit
            page = 1
        page_obj = Paginator(obj_list,limit)
        result = page_obj.get_page(page)#分好页的数据
        return page_obj,result #page=1,limit=10,page=2



class GetView:
    def get(self, request):
        l = []
        interface_query_set = self.model_class.objects.filter(**self.get_filter_dict).filter(self.get_search_obj)

        page_obj, page_data = self.get_page_data(interface_query_set)

        for d in page_data:
            dic = model_to_dict(d, fields=self.show_field, exclude=self.exclude_field)
            l.append(dic)

        return SkyResponse(data=l, count=page_obj.count)

class PostView:
    def post(self,request):
        form = self.form_class(request.POST)
        if form.is_valid():#是否校验通过
            self.model_class.objects.create(**form.cleaned_data)
            return SkyResponse()
        else:
            return SkyResponse(code=-1,msg=form.format_errors)

class DeleteView:
    def delete(self,request):
        id = request.GET.get('id')
        if id:
            query_set = self.model_class.objects.filter(id=id)
            if query_set:
                query_set.delete()
                return SkyResponse()
            else:
                return SkyResponse(code=404, msg='id不存在')
        else:
            return SkyResponse(code=-1,msg='请传入id')

class PutView:
    def put(self,request):
        id = request.PUT.get('id')
        if id:
            query_set = self.model_class.objects.filter(id=id)
            if query_set:
                instance_obj = query_set.first()
                form = self.form_class(request.PUT,instance=instance_obj,files=request.FILES) #代表
                if form.is_valid():  # 是否校验通过
                    form.save() #自动把修改的保存到数据库
                    return SkyResponse()
                else:
                    return SkyResponse(code=-1, msg=form.format_errors)
            else:
                return SkyResponse(code=404, msg="要修改的数据不存在")

        else:
            return SkyResponse(code=-1,msg="id不能为空")





if __name__ == "__main__":
    pass
