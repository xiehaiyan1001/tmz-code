# Generated by Django 2.1.7 on 2020-12-27 16:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0007_interface'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sku',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='修改时间')),
                ('name', models.CharField(max_length=50, verbose_name='工具名称')),
                ('price', models.IntegerField(verbose_name='价格')),
                ('count', models.IntegerField(default=50, verbose_name='数量')),
                ('color', models.CharField(max_length=50, verbose_name='颜色')),
            ],
            options={
                'verbose_name': '商品表',
                'verbose_name_plural': '商品表',
                'db_table': 'sku',
                'ordering': ['-update_time'],
            },
        ),
        migrations.AlterModelOptions(
            name='interface',
            options={'verbose_name': '接口', 'verbose_name_plural': '接口'},
        ),
    ]
