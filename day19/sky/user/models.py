from django.db import models

# Create your models here.

#分类表、文章表

class BaseModel(models.Model):
    create_time = models.DateTimeField(verbose_name='创建时间',auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='修改时间',auto_now=True)

    class Meta:
        abstract = True


class Category(BaseModel):
    name = models.CharField(verbose_name='分类名称',max_length=50,unique=True)

    class Meta:
        db_table = 'category'
        verbose_name = '文章分类'
        verbose_name_plural = verbose_name
        ordering = ['-create_time']

    def __str__(self):
        return self.name

class Article(BaseModel):
    title = models.CharField(verbose_name='文章标题',max_length=100,)
    content = models.TextField(verbose_name='文章内容') #长文本类型
    read_count = models.IntegerField(verbose_name='阅读次数',default=0)
    category = models.ForeignKey(Category,on_delete=models.PROTECT,verbose_name='分类')  #外键
    #models.DO_NOTHING #什么也不干
    #models.CASCADE ,会删除
    #models.SET_DEFAULT ，设置一个默认值，1
    #models.SET_NULL #设置成空
    #models.PROTECT#受保护的，如果还有在使用的，不能让你删除
    #models.SET#自定义模式，自己指定

    class Meta:
        db_table = 'article'
        verbose_name = '文章'
        verbose_name_plural = verbose_name
        ordering = ['-create_time']

    def __str__(self):
        return self.title

class Teacher(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'teacher'

class Student(models.Model):
    name = models.CharField(max_length=50,verbose_name='学生姓名')
    teacher = models.ManyToManyField(Teacher,verbose_name='老师')

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'student'

class Case(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=10)
    method = models.CharField(max_length=20)
    param = models.CharField(max_length=30)
    require_case = models.ForeignKey('self',on_delete=models.PROTECT,verbose_name='依赖用例',null=True,blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'case'
        ordering = ['-id']



class Account(models.Model):
    balance = models.IntegerField(verbose_name='余额',default=0)

    class Meta:
        db_table = 'account'

class User(models.Model):
    name = models.CharField(max_length=50)
    account = models.OneToOneField(Account,on_delete=models.CASCADE,verbose_name='账户')#级联

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'user'


class Interface(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=50,unique=True)
    response = models.TextField(default='{}',null=True,blank=True,verbose_name='返回报文')


    def __str__(self):
        return self.name

    class Meta:
        db_table = 'interface'
        verbose_name = "接口"
        verbose_name_plural = verbose_name


class Sku(BaseModel):
    name = models.CharField(max_length=50,verbose_name='工具名称')
    price = models.IntegerField(verbose_name='价格',)
    count = models.IntegerField(verbose_name='数量',default=10,null=True,blank=True)
    color = models.CharField(max_length=50,verbose_name='颜色')

    class Meta:
        verbose_name = '商品表'
        verbose_name_plural = verbose_name
        ordering = ['-update_time']
        db_table = 'sku'




