# -*- coding:utf-8 -*-
# @FileName  :forms.py
# @Time      :2020-12-27 11:36
# @Author    :niuhanyang
import json

from django.forms import Form
from django import forms
from . import models
from common.sky_form import ExtendForm



class InterfaceForm2(forms.ModelForm,ExtendForm):

    class Meta:
        model = models.Interface #指定model
        fields = '__all__'  #如果是所有的字段就写 __all__ ，  指定字段就写list['name','url]
        #exclude = ['response'] #排除哪些字段
        #fields或者exclude这两个必须写一个
        #多加字段也是可以的

    def clean_response(self):
        response = self.data.get('response','{}')
        try:
            json.loads(response)
        except:
            self.add_error('response', 'json不合法')
        return response


class UserForm(forms.ModelForm,ExtendForm):
    class Meta:
        model = models.User
        fields = '__all__'


class InterfaceForm(Form,ExtendForm):
    name = forms.CharField(max_length=50,min_length=1)
    url = forms.CharField(max_length=50,min_length=1) #是唯一的
    response = forms.CharField(required=False,initial='{}') #校验它必须是一个json

    #接口1
    # url interface

    #钩子函数
    def clean_url(self):
        #self.data 原始的数据
        url = self.data.get('url')
        if models.Interface.objects.filter(url=url):
            self.add_error('url','已经存在')
        return url

    def clean_response(self):
        response = self.data.get('response')
        try:
            json.loads(response)
        except:
            self.add_error('response', 'json不合法')
        return response

    def clean(self): #校验多个参数一起校验的话，那就用clean，clean里面是不需要写返回值的。
        name = self.data.get('name')
        url = self.data.get('url')
        if models.Interface.objects.filter(name=name,url=url):
            self.add_error('name','相同的name和url已经存在')
            self.add_error('url','相同的name和url已经存在')



class MyForm(Form,ExtendForm):
    type = forms.ChoiceField(choices=(1,2))
    message = forms.CharField() #字符串 # empty_value
    age = forms.IntegerField() #int


class SkuForm(forms.ModelForm,ExtendForm):
    class Meta:
        model = models.Sku
        fields = '__all__'














if __name__ == "__main__":
    pass
