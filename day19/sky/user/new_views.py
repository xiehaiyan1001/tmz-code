# -*- coding:utf-8 -*-
# @FileName  :new_views.py
# @Time      :2020-12-06 15:51
# @Author    :niuhanyang
import json

from common.responses import SkyResponse
from common.utils import model_to_dict
from . import models
from . import forms
from common.custom_view import BaseView,GetView,PostView
import hashlib


class InterfaceView(BaseView):
    filter_field = ['id','name']
    search_field = ['name','url','response']
    model_class = models.Interface
    form_class = forms.InterfaceForm2


class UserView(BaseView,GetView):
    model_class = models.User
    filter_field = ['id','name']
    form_class = forms.UserForm


class SkuView(BaseView,GetView,PostView):
    model_class = models.Sku
    filter_field = ['id','name','count','color','price']
    form_class = forms.SkuForm
    exclude_field = ['price']



class ArticleView(BaseView,GetView):
    model_class = models.Article

    def get(self, request):
        l = []
        interface_query_set = self.model_class.objects.filter(**self.get_filter_dict).filter(self.get_search_obj)

        page_obj, page_data = self.get_page_data(interface_query_set)

        for d in page_data:
            dic = model_to_dict(d, fields=self.show_field, exclude=self.exclude_field)
            dic['category_name'] = d.category.name
            l.append(dic)

        return SkyResponse(data=l, count=page_obj.count)





def jiami(request):
    message = request.GET.get("message",'')
    m = hashlib.md5(message.encode())
    result = m.hexdigest()
    return SkyResponse(data=result)






#增删改查