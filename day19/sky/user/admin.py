from django.contrib import admin
from . import models
# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id','name','create_time','update_time']
    list_filter = ['name']
    search_fields = ['name']


admin.site.register(models.Category,CategoryAdmin)
admin.site.register(models.Article)
admin.site.register(models.Student)
admin.site.register(models.Teacher)
admin.site.register(models.Case)
admin.site.register(models.User)
admin.site.register(models.Account)
admin.site.register(models.Interface)

admin.site.site_title = "sky"
admin.site.site_header = "sky后台管理"