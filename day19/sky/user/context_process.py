# -*- coding:utf-8 -*-
# @FileName  :context_process.py
# @Time      :2020-11-29 11:18
# @Author    :niuhanyang

from . import models

def category_process(request):
    #先走到view，然后再走到上下文管理器
    categories = models.Category.objects.all()
    return {"title":"天马座博客",'categories':categories }

def tag_process(request):
    tag_list = ['python','django','自动化测试']
    return {'tag_list':tag_list}


if __name__ == "__main__":
    pass
