# -*- coding:utf-8 -*-
# @FileName  :urls.py
# @Time      :2020-12-06 14:08
# @Author    :niuhanyang

from . import views

from django.urls import path

urlpatterns = [
    path('user',views.UserView.as_view(),),
]


if __name__ == "__main__":
    pass
