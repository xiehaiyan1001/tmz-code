from django.db import models

# Create your models here.


class Nhy(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'niuhanyang'

class Interface(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=50,unique=True)
    response = models.TextField(default='{}',null=True,blank=True,verbose_name='返回报文')


    def __str__(self):
        return self.name

    class Meta:
        db_table = 'interface'