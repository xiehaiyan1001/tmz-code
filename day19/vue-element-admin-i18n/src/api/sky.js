import request from '@/utils/request'

export function get_user_list() {
  return request({
    url: '/user/query', // 假地址 自行替换
    method: 'get'
  })
}

export function add_user(data) {
  return request({
    url: '/user/', // 假地址 自行替换
    method: 'put',
    data: data
  })
}
