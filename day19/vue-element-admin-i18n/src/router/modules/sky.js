/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const skyRouter = {
  path: '/sky',
  component: Layout,
  children: [
    {
      path: 'index',
      component: () => import('@/views/sky/user_list'),
      name: 'sky',
      meta: { title: '天马座', icon: 'people', affix: true }
    }
  ]
}

export default skyRouter
