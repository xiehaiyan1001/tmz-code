#username
#grade
#phone
#addr
#age
l = ['xiaohei','天马座','112342','北京','29']

d = {
    'username':'xiaohei',
    'id':1,
    'grade':'天马座',
    'addr':'北京',
    'age':18
}

# d['addr'] = 'shanghai'
# d.setdefault('age',38)
#
#
# d['addr'] = 'shanghai'

# print(d['addr'])
# print(d.get('sex','男'))
#
#
# d.pop('username')
# del d['addr']
print(d.keys())
print(d.values())
d.update(a=1,b=2)

# d.clear()#清空
print(d)

users = {
    'fd':'123455',
    'xzh':'456789'
}

username = 'fd'
# print(d.keys())
# print(username in users.keys())
# print(username in users) #判断这个key是否存在的
#users.has_key(username) #python2

print(d.items())
# [('username', 'xiaohei'), ('id', 1), ('grade', '天马座'), ('addr', '北京'), ('age', 18), ('a', 1), ('b', 2)]
# for k,v in d.items():
#         print(k,v)

for k in d:
    print(k,d.get(k))