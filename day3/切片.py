#切片是list范围取值的一种方式

import string
# print(string.ascii_lowercase)
# print(string.ascii_uppercase)
# print(string.digits)
# print(string.ascii_letters)
# print(string.punctuation)
#    0     1    2    3   4    5     6   7    8
l = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']#切片的时候是顾头不顾尾的
#   -9    -8    -7   -6   -5   -4   -3   -2   -1
# print(l[0:9:2]) #步长
# print(l[::-1])  #如果是负数的话，那么取值的时候就从右往左开始取，同时你的开始和结束下标也要写成负数
# print(l[-1:-4:-1])

# s='abcdef'
# print(s[::-1])
import copy
# print(list(range(10)))
#    0  1  2 3  4  5  6
# l = [0, 1, 2,2, 3, 4, 5]
# # l2 = copy.deepcopy(l)#深拷贝
# l2 = l.copy()


# print(id(l))
# print(id(l2))
# for i in l2:
#     if i % 2 == 0:
#         l.remove(i)
# print(l)

l = [ 1,2,3,[4,5,6] ]
# l2 = l.copy()
# l2 = copy.copy(l)

l2 = l[:]

print(id(l))
print(id(l2))
l[-1].append('abc')
print(l)
print(l2)

#浅拷贝/深拷贝 #如果复制了一个变量，这两个变量其中一个变了之后，不应该影响另外一个的情况下，就要用深拷贝