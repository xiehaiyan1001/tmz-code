# -*- coding:utf-8 -*-
# @FileName  :forms.py
# @Time      :2021-01-10 13:38
# @Author    :niuhanyang
from django.db.models import Q

from . import models
from common.sky_form import ExtendForm
from django.forms import ModelForm
from django import forms
from common.utils import md5,model_to_dict
from django.conf import settings
import time,pickle
from common.utils import get_redis
from user import const

class UserForm(ModelForm,ExtendForm):
    phone = forms.CharField(max_length=11,min_length=11,label="手机号")
    password = forms.CharField(min_length=6,max_length=18)

    class Meta:
        model = models.User
        fields = '__all__'

    def clean_password(self):
        password = self.data.get("password")
        new_password = md5(password,settings.SECRET_KEY)
        return new_password



class LoginForm(forms.Form,ExtendForm): #username,password
    username = forms.CharField(min_length=8) #phone emaill
    password = forms.CharField(min_length=6,max_length=18)

    # 生成token，塞到redis里面
    # 1、token按照什么规则来生成  #userid+time.time()
    # 2、写操作redis的函数
    # 3、如果重复登录怎么办
    #form.is_invaid()

    def check_login(self,user):
        #校验是否重复登录
        r = get_redis()
        userid_md5 = md5(user.id)
        user_id_key = const.user_id_prefix+userid_md5
        if r.exists(user_id_key):
            token = r.get(user_id_key).decode()
            r.expire(user_id_key,const.token_expire_time) #更新userid的key过期时间
            r.expire(const.token_prefix+token,const.token_expire_time) #更新token的过期时间
            return token

    def create_token(self,user):#生成token
        token = md5(str(user.id) + str(time.time()), settings.SECRET_KEY)
        userid_md5 = md5(user.id)
        user_pickle = pickle.dumps(user)  # 序列化user对象，要把user对象存到redis里面
        r = get_redis()
        r.set(const.token_prefix + token, user_pickle, const.token_expire_time) #set token对应用户信息
        r.set(const.user_id_prefix+userid_md5,token,const.token_expire_time) #set userid对应的token，是用来校验是否重复登录的
        return token


    def clean(self):
        username = self.data.get("username")
        password = self.data.get("password")
        user_result = models.User.objects.filter(Q(phone=username)|Q(email=username))
        if user_result:
            user = user_result.first()
            md5_password = md5(password,settings.SECRET_KEY)
            if user.password == md5_password:
                #userid -》 token
                token = self.check_login(user)
                if token: #重复登录
                    self.token = token
                else:#没有重复登录
                    self.token = self.create_token(user)

            else:
                self.add_error("password","密码不正确")
        else:
            self.add_error("username","手机号/邮箱不存在！")



class ChangePwdForm(forms.Form,ExtendForm):
    password = forms.CharField(min_length=6, max_length=18)
    new_password = forms.CharField(min_length=6, max_length=18)


