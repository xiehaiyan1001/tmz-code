# -*- coding:utf-8 -*-
# @FileName  :urls.py
# @Time      :2021-01-10 13:42
# @Author    :niuhanyang


from django.urls import path
from . import views

urlpatterns = [
    path('register', views.RegisterView.as_view()),
    path('login', views.LoginView.as_view()),
    path('logout', views.Logout.as_view()),
    path('change_pwd', views.ChangePwdView.as_view()),
    path('upload', views.UploadView.as_view()),

]


if __name__ == "__main__":
    pass
