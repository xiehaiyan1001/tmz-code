# -*- coding:utf-8 -*-
# @FileName  :const.py
# @Time      :2021-01-10 14:35
# @Author    :niuhanyang

token_expire_time = 60 * 60 * 24 * 7 #token过期时间
token_prefix = "token:"
user_id_prefix = "userid:"

if __name__ == "__main__":
    pass
