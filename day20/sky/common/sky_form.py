# -*- coding:utf-8 -*-
# @FileName  :sky_form.py
# @Time      :2020-12-27 14:56
# @Author    :niuhanyang


class ExtendForm:

    @property
    def format_errors(self):
        error_message_list = []
        for field,errors in self.errors.get_json_data().items():
            message = errors[0].get('message')
            msg = field + message #url已经存在，name不能重复，response不合法
            error_message_list.append(msg)

        return ','.join(error_message_list)




if __name__ == "__main__":
    pass
