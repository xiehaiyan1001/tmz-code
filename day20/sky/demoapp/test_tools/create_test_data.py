# -*- coding:utf-8 -*-
# @FileName  :create_test_data.py
# @Time      :2021-01-10 11:11
# @Author    :niuhanyang

import random
import string

def create_open_account(count):
    data_list = []
    while len(data_list)!=count:
        account = ''.join(random.sample(string.ascii_letters+string.digits,8))
        if account not in data_list:
            data_list.append(account+'@163.com')
    return data_list