# -*- coding:utf-8 -*-
# @FileName  :urls.py
# @Time      :2021-01-10 10:30
# @Author    :niuhanyang

from django.urls import path
from . import views

urlpatterns = [
    path('book', views.BookView.as_view()),
    path('author', views.AuthorView.as_view()),
    path('test_data', views.TestAccountView.as_view()),
]
