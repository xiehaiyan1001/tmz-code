# -*- coding:utf-8 -*-
# @FileName  :form.py
# @Time      :2021-01-10 10:20
# @Author    :niuhanyang

from django.forms import ModelForm,Form
from django import forms
from common.sky_form import ExtendForm
from . import models

class BookForm(ModelForm,ExtendForm):

    class Meta:
        model = models.Book
        fields = '__all__'

class AuthorForm(ModelForm,ExtendForm):

    class Meta:
        model = models.Author
        fields = '__all__'

class TestAccountForm(Form,ExtendForm):
    count = forms.IntegerField(min_value=1)




if __name__ == "__main__":
    pass
