from django.db import models

# Create your models here.


class Author(models.Model):
    name = models.CharField(max_length=50,verbose_name='姓名')

    class Meta:
        db_table = 'nhy_author'

    def __str__(self):
        return self.name

class Book(models.Model):
    name = models.CharField(max_length=50,verbose_name='书名')
    count = models.IntegerField(verbose_name='数量')
    price = models.IntegerField(verbose_name='价格')
    author = models.ForeignKey(Author,on_delete=models.DO_NOTHING,verbose_name="作者")
    create_time = models.DateTimeField(auto_now_add=True,verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True,verbose_name='修改时间')

    class Meta:
        db_table = 'nhy_book'

    def __str__(self):
        return self.name