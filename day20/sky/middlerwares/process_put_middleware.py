# -*- coding:utf-8 -*-
# @FileName  :process_put_middleware.py
# @Time      :2020-12-27 16:02
# @Author    :niuhanyang
from django.http import QueryDict
from django.middleware.common import MiddlewareMixin


class PutMiddleWare(MiddlewareMixin):
    def process_request(self,request):
        if request.method.upper() == 'PUT': #如果是put请求
            contenttype = request.META.get('CONTENT_TYPE')  #获取请求参数的类型
            if 'multipart' in contenttype: #如果是带有文件的这种
                put_data, files = request.parse_file_upload(request.META, request)
                request.PUT = put_data
                request._files = files
            else:#
                put_data = QueryDict(request.body)
                request.PUT = put_data




if __name__ == "__main__":
    pass
