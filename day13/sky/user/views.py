from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.http.response import JsonResponse
from . import models
# Create your views here.

from hashlib import md5
def sign(s):
    news = 'sdgsdgsgsdgsdx&'+s
    m = md5(news.encode())
    return m.hexdigest()



def index(request):
    #获取请求参数
    categories = models.Category.objects.all()
    articles = models.Article.objects.all()
    title = "my blog"
    return render(request,'index.html',
                  {'title':title,'categories':categories,'articles':articles})

def category(request,id):
    articles = models.Article.objects.filter(category_id=id)
    return render(request,'category.html',{'articles':articles})


def get_sign(request):
    s = request.GET.get('str')
    result = sign(s)
    data = {"code":0,"data":result}
    return JsonResponse(data)


def category_view(request):
    #查询
    # c = models.Category.objects.all() #查询所有的数据
    # for i in c:
    #     print(i.name)
    #     print(i.create_time)
    #     print(i.update_time)
    # print("=======")
    # j = models.Category.objects.get(name="java")#单条数据
    # #必须保证返回的是一条
    # print(j.name)
    #
    # f = models.Category.objects.filter(name='java',id=3)
    # print(f)
    #
    # result = models.Category.objects.filter(name="mysql").exists() #是否存在
    # print(result)
    #
    # result = models.Category.objects.filter(name="mysql").count()
    # #返回多少条
    # print(result)



    c = models.Category.objects.get(id=3)#分类-》文章
    print(c.article_set.count()) #分类下面的所有的文章
    print(c.article_set.all()) #分类下面的所有的文章
    print(c.article_set.filter()) #分类下面的所有的文章


    art = models.Article.objects.get(id=1)#修改
    art.read_count+=1
    art.save()



    return HttpResponse("ok")

