# -*- coding:utf-8 -*-
# @FileName  :sign.py
# @Time      :2020-11-15 09:38
# @Author    :niuhanyang

#1、第一种
from hashlib import md5
def sign(s):
    news = 'sdgsdgsgsdgsdx&'+s
    m = md5(news.encode())
    return m.hexdigest()
#2、

import flask,json

app = flask.Flask(__name__)


#前后端不分离
@app.route('/',methods=['post','get'])
def index():
    if flask.request.method.upper() == 'GET':
        f = open('test.html',encoding='utf-8')
        result = f.read().replace('{username}','niuhanyang')
        return result
    else:
        s = flask.request.values.get('str')
        sign_str = sign(s)
        return "签名是"+sign_str

@app.route('/test')
def index2():
    s = flask.request.values.get('str')
    if s:
        sign_str = sign(s)
        return json.dumps({"sign":sign_str})
    return json.dumps({"msg":"请求参数为空"})


app.run(debug=True)