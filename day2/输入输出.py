print('hello world!')
name = "小黑" #字符串 str
age = 18 #整数类型 int
score  = 97.5 #浮点型 float

words = " let'go "
words2 = ' tom very "shy" ' #
# let'go,tom very "shy"
words3 = ''' let'go,tom very "shy" '''
# print(words)
# print(words2)
# print(words3)

'''
your_name = input("请输入你的名字：")
#python2里面用raw_input
print('你的名字是',your_name)
'''

# 名字 = '小黑'
# 输出 = print
#
# 输出(名字)
#
# 输出("今天是星期天，我们在上课，为啥不能出去玩")