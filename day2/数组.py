students = "朱成，露津健，王祥龙"

#数组、列表、list
students_new = [ '朱成','露津健','王祥龙','朱成']
#0                0      1       2
#编号 ，索引、下标、角标
# print(students_new[1] )
#增
students_new.append('李韩韩')#在列表的末尾添加元素
students_new.insert(0,'刘海洋')
#修改
students_new[2] = '鲁津健'
#删除
# students_new.pop(1)#传入指定的下标，删除元素
# students_new.remove('李韩韩')#删除指定的元素
# del students_new[-1]

# print(students_new)
# # print(students_new[3])
# print(students_new[-1])
# print(students_new[0])

# students_new.clear() #清空list
# print('清空之后的',students_new)

zc_count = students_new.count('朱成')#统计这个list里面元素出现的次数
# print('count',zc_count)
# print('index',students_new.index('朱成'))#找到某个元素的下标

users = ['高雯','代爽']
students_new.extend(users)#把一个列表里面的元素加入另一个列表里面
print('reverse之前的',students_new)
# students_new.reverse()#反转list 1,2,3,4,5,6 654321
# print('reverse之后的',students_new)

numbers = [9,23,52323,235,235,2,346,3463,36]
numbers.sort(reverse=True)#排序,默认是升序
print(numbers)

#多维数组
student_info = [
    [1,'刘海洋','北京'],
    [2,'hzy','shanghai'],
    [3,'ljj','天津']
]#二维数组
student_info2 = [
    [1,'刘海洋','北京',['bmw','benz','audi'] ],
    [2,'hzy','北京',['bmw','benz','audi']],
    [3,'ljj','北京']
] #三维数组
student_info2.remove('北京')
# student_info[0][-1] = '山东'
# student_info2[0][-1].append('tesla')
# student_info2[0].pop(2)
# student_info2[2].append(['wlhg','yfnd'])
# print(student_info2)