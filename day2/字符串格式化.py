#xxx,欢迎登陆
#xxx，今天要喝8杯水
import datetime
name = '高雯'
welcome = '高雯，欢迎登陆'
today = datetime.datetime.today()
#字符串格式化：
    #1、简单粗暴直接 +
print(name + ',欢迎登陆.'+'今天的日期是' + str(today) )#不推荐
#2、占位符,%s %d %f
# welcome = '%s,欢迎登陆,今天的日期是 %s' % (name,today)
# print(welcome)
#
# age = 18.78
# age_str = '你的年龄是 %d' % age
# score = 73.98312
# score_str = '你的成绩是 %.2f' % score
# print(age_str)
# print(score_str)

#3.大括号的方式
name = 'lily'
phone = '1381232523'
grade = 'tmz'
money = 5000
score = 98.133
addr = "北京"

name2='李韩韩'

sql = 'insert into students values ("%s","%s","%s"' \
      ',"%d","%.2f","%s");' % (phone,name,grade,money,score,addr)

welcome = '{name},欢迎登陆,今天的日期是{today}'.format(today=today,name=name2 )
welcome2 = '{},欢迎登陆,今天的日期是{}'.format(today,name)
print(welcome)
print(welcome2)