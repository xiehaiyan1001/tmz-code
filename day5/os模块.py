import os

# print(os.listdir('/Users/nhy/PycharmProjects/tmz-code') )#获取某个目录下的内容

# os.mkdir('python')
# os.mkdir('python/day2')
# os.mkdir('python/day3')
# os.makedirs('java/day1')
# os.makedirs('java/day2')
# os.makedirs('java/day3') #父目录不存在的时候，会创建父目录

# os.remove('python') #删除文件，不能删除文件夹
# os.rmdir('java')#删除空文件夹的
# os.rename('java','python') #重命名，文件和文件夹都可以
# print(os.getcwd())#获取当前所在的目录

# os.chdir('/Users/nhy/PycharmProjects/tmz-code')#进入到某个目录里面
# os.mkdir('java')
# print(os.getcwd())
# print(os.environ)#获取系统环境变量里面配置的内容

# result = os.system('sgw3dsgs') #执行操作系统命令的,它只能帮你执行，不能拿到命令的结果
#它返回的是命令执行是否成功，如果返回的是0，代表执行成功
# print('!!!!!!!!!',result)
# result = os.popen('ifconfig').read()
# print('result,,',result)

# os.path.join()
# os.path.abspath()
# os.path.split()
# os.path.dirname()
# os.walk()

# print(os.path.sep)#当前系统的路径分隔符
# print(os.path.isfile('a.py')) #是否为文件
# print(os.path.isdir('a.py')) #是否为文件夹
# print(os.path.exists('a.py')) #文件/文件夹是否存在
# print(os.path.getsize('a.py')) #获取大小
# print(os.path.getctime('a.py')) #创建时间
# print(os.path.getmtime('a.py')) #修改时间
# print(os.path.getatime('a.py')) #最后一个访问时间

# print(os.path.split('/Users/nhy/PycharmProjects/tmz-code/day5/a.py'))
#分隔路径和文件名的

# p = 'e:'+os.path.sep+'movies'+os.path.sep+'欧美大片'
# print(p)
# print(os.path.join('e:','movies','欧美大片','复仇者联盟.mp4'))
#拼接路径
# print(os.path.abspath(r'../day4/a.json')) #把相对路径，转换成绝对路径
#/Users/nhy/PycharmProjects/tmz-code/day4/a.json
#
# print(os.path.dirname(r'/Users/nhy/PycharmProjects/tmz-code/day4/a.json') )#取父目录
#e:\xxx\python\sqls
for cur_path,dirs,files in os.walk(r'/Users/nhy/'):
    print('当前在%s目录下查找'%cur_path)
    for file in files:
        if file.endswith('.mp4') or file.endswith('.avi'):
            print('发现小电影在%s目录下'%cur_path)
            break