import os,random,sys,time,string

# print(random.randint(1,10))
# print(random.uniform(1,10))
#
# print(random.choice(string.ascii_lowercase))#随机选择一个元素
# print(random.sample(string.ascii_lowercase,4))
#
# l=[random.choice(string.digits)  for i in range(6)]
# l2 = random.sample(string.digits,6) #取的这几个元素里面，它不会有重复的
#
# print(''.join(l))
# print(''.join(l2))

l = [1,2,3,4,5,6,7,8]
print('打乱之前的',l)
random.shuffle(l)
print('打乱之后的',l)

s='235423236236'

result = sorted(s,reverse=True)
print(result)



# vs = sorted(d.values(),reverse=True)
# for v in vs:
#     for key,value in d.items():
#         if v==value:
#             print(key,v)

def get_value(x):
    return x[1]
d = {
    'fd':100,
    'ds':93,
    'lhy':88,
    'hzy':35
}
result = sorted(d.items(),key=lambda x:x[1],reverse=True)
# result = sorted(d.items(),key=get_value,reverse=True)

print(result)