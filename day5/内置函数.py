#coding=utf-8

# list dict tuple str int float set
# print input type id len
#
# print(all([1, 2, 3, 4]))  # 判断可迭代的对象里面的值是否都为真
# print(any([0, 1, 2, 3, 4]))  # 判断可迭代的对象里面的值是否有一个为真
# print(max(111, 12))  # 取最大值
# print(round(11.11, 2))  # 取几位小数
# print(min([1,2,3,4]))
# print(filter(lambda x: x > 5, [12, 3, 12, 2, 1, 2, 35]))  # 把后面的迭代对象根据前面的方法筛选
# print(map(lambda x: x > 5, [1, 2, 3, 4, 5, 6]))

# print(bin(10))  # 十进制转二进制
# print(chr(10))  # 打印数字对应的ascii
# print(ord('b'))  # 打印字符串对应的ascii码
# print(dir(1))  # 打印传入对象的可调用方法
# print(eval('[]'))  # 执行python代码，只能执行简单的，定义数据类型和运算
# print(exec('def a():pass'))  # 执行python代码
# print(globals())  # 返回程序内所有的变量，返回的是一个字典
# print(locals())  # 返回局部变量
# print(hex(111))  # 数字转成16进制
# print(oct(111))  # 把数字转换成8进制


a = b = c = False



# print(all([1,2,3,4,False]))#判断可迭代的对象里面的值是否都为真

# print(any([0,False,'',[],1]))
# print(max([1,2,3,4]))
# print(min([1,2,3,4]))
# print(round(11.11988, 2))  # 取几位小数
# print(dir(a))

# print(bin(10))  # 十进制转二进制

# print(chr(43))  # 打印数字对应的ascii
# print(ord('+'))  # 打印字符串对应的ascii码
# print(hex(111))  # 数字转成16进制
# print(oct(111))  # 把数字转换成8进制

s='''
import os
for i in range(10):
    print(i)
'''
# exec(s)#用来动态执行python代码的

# f = open('a.json',encoding='utf-8')

# result = eval(f.read()) #用来动态执行python代码的,简单的
# print(result)

filter  #过滤
map #

def oushu(number):
    if number%2==0:
        return True

l = range(1,11)
l2 = []
for i in l:
    if oushu(i):
        l2.append(i)

result = list(filter(oushu,l))
result2 = list(map(oushu,l))
print(l2)
print(result)
print(result2)

#filter会自动循环你传给他的list，然后把list里面的
#每一个元素传给指定的函数，如果这个函数结果返回的是
#true，那么就保留这个元素

#filter会自动循环你传给他的list，然后把list里面的
#每一个元素传给指定的函数，把这个函数返回的结果保存下来

result3 = list(map(str,range(1,101)))

# print(result3)

def test():
    a = 1
    b = 2
    print(locals())#获取当前函数里面的局部变量
    print(globals())#获取当前文件里面的全局变量


a = ['fd','hzy','ds']
b = ['1224','456','789']
c = ['1','2','3']
[
    ['fd','1224','1'],
    ['hzy','456','2'],
    ['ds','789','3'],
]
# for username,password,c in zip(a,b,c):
#     print(c,username,password)
for u in zip(c,a,b):
    print(u)

# len type id print input open
# round min max filter map zip exec eval sorted
