# -*- coding:utf-8 -*-
# @FileName  :urls.py
# @Time      :2020-12-06 14:13
# @Author    :niuhanyang

from django.urls import path
from . import views,new_views

urlpatterns = [
    path('index', views.index),
    path('', views.index),
    path('nhy', views.nhy),
    path('test', views.test),
    path('add', views.add_article),
    path('category/<int:id>', views.category),
    path('detail/<int:id>', views.article),

    path('interface', new_views.InterfaceView.as_view() ),
    path('user', new_views.UserView.as_view() ),



]


if __name__ == "__main__":
    pass
