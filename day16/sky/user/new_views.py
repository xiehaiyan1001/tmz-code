# -*- coding:utf-8 -*-
# @FileName  :new_views.py
# @Time      :2020-12-06 15:51
# @Author    :niuhanyang
from django.forms import model_to_dict
from django.views import View
from common.responses import SkyResponse
from . import models
from django.db.models import Q
from django.core.paginator import Paginator
from common.const import page_limit

class BaseView(View):
    filter_field = []
    search_field = []
    model_class = None

    @property
    def get_filter_dict(self):
        filter_dict = {}
        #{id:1,name:lhy}
        for field in self.filter_field:
            value = self.request.GET.get(field)
            if value:
                filter_dict[field] = value
        return filter_dict

    @property
    def get_search_obj(self):
        q_obj = Q()#空白的q对象
        search = self.request.GET.get('search')
        if search:
            for field in self.search_field:
                d = {'%s__contains' % field:search}
                q_obj = q_obj | Q(**d) #这是每次在拼Q对象，生成or条件
        return q_obj

    # 'select * from interface where url like 'aa' or name like 'aa' or response like 'aa';

    # models.Interface.objects.filter( Q(url__contains=search)
    #                                  |Q(name__contains=search)|
    #                                  Q(response__contains=search) )

    def get_page_data(self,obj_list):
        try:
            limit = int(self.request.GET.get('limit',page_limit)) #limit = s
            page = int(self.request.GET.get('page',1))
        except:
            limit = page_limit
            page = 1
        page_obj = Paginator(obj_list,limit)
        result = page_obj.get_page(page)#分好页的数据
        return page_obj,result #page=1,limit=10,page=2


    def get(self, request):
        # get 一条？id=1 筛选
        # get 多条？all
        # get 模糊查询 name=登
        # 分页
        l = []
        interface_query_set = self.model_class.objects.filter(**self.get_filter_dict).filter(self.get_search_obj)

        page_obj, page_data = self.get_page_data(interface_query_set)

        for d in page_data:
            dic = model_to_dict(d)
            l.append(dic)

        return SkyResponse(data=l, count=page_obj.count)

    def delete(self,request):
        id = request.GET.get('id')
        if id:
            query_set = self.model_class.objects.filter(id=id)
            if query_set:
                query_set.delete()
                return SkyResponse()
            else:
                return SkyResponse(code=404, msg='id不存在')
        else:
            return SkyResponse(code=-1,msg='请传入id')


class InterfaceView(BaseView):
    filter_field = ['id','name']
    search_field = ['name','url','response']
    model_class = models.Interface

class UserView(BaseView):
    model_class = models.User
    filter_field = ['id','name']





#增删改查