# -*- coding:utf-8 -*-
# @FileName  :test_middlerwares.py
# @Time      :2020-12-06 14:29
# @Author    :niuhanyang

from django.middleware.common import MiddlewareMixin
from django.shortcuts import HttpResponse
from user.models import Interface

class TestMiddleWare(MiddlewareMixin):
    def process_request(self,request):
        #/pay
        path = request.path_info.replace('/','',1)
        #pay
        interface = Interface.objects.filter(url=path)
        if interface:
            interface = interface.first()
            return HttpResponse(interface.response)

    def process_response(self,request,response):
        print('process_response')
        return response

    def process_exception(self,request,exception):
        print(exception)
        return HttpResponse("系统开小差了~")


if __name__ == "__main__":
    pass
