# -*- coding:utf-8 -*-
# @FileName  :database_router.py
# @Time      :2020-12-06 11:21
# @Author    :niuhanyang


db_mapper = {
    'sqlite':'default',
    'order':'mysql'

}

class DatabaseAppsRouter(object):
    def db_for_read(self, model, **hints):
        app_label = model._meta.app_label
        if app_label in db_mapper:
            return db_mapper[app_label]
        return 'default'

    def db_for_write(self, model, **hints):
        app_label = model._meta.app_label
        if app_label in db_mapper:
            return db_mapper[app_label]
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        '''允许两个表有关系'''
        return True

        # ValueError: Cannot
        # assign
        # "<ContentType: ContentType object (1)>": the
        # current
        # database
        # router
        # prevents
        # this
        # relation.


if __name__ == "__main__":
    pass
