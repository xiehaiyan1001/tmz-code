
# f = open('user.txt')
# f.close()

# with open('user.txt',encoding='utf-8') as f: #文件对象，文件句柄
#     for line in f:
#         line = line.strip()
#         if line:
#             print(line)

#1、读取到文件所有内容
#2、替换 new_str
#3、清空原来的文件
#4、写进去新的


#新的
import os

with open('words.txt') as fr,open('words_new.txt','w') as fw:
    for line in fr:
        line = line.strip()
        if line:
            line = line.upper()
            fw.write(line+'\n')
os.remove('words.txt')
os.rename('words_new.txt','words.txt')