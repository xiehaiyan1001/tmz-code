age = 18

if age<18:
    v = '未成年人'
else:
    v = '成年人'

v = '未成年人' if age < 18 else '成年人'

a = [1,2,3,4,5,6]
b = []
for i in a:
    b.append(str(i))

c = [ str(i) for i in a]
d = [ str(i) for i in a if i%2!=0 ]