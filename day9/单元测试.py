import unittest
import HTMLTestRunner
import HTMLTestRunnerNew
import parameterized

def add(a,b):
    return a+b

def login():
    return 'token'

class TestAdd(unittest.TestCase):
    '''测试add方法'''

    @classmethod
    def setUpClass(cls):
        print("setUpClass")

    @classmethod
    def tearDownClass(cls):
        print("tearDownClass")

    def test_add_normal(self):
        '''正常测试加法的，by huoziyang'''
        result = add(1,2)
        self.assertEqual(3,result)

    def test_add_error1(self):
        '''测试失败使用'''
        result = add(1,2)
        self.assertEqual(4,result)

    def test_add_error2(self):
        '''测试失败有msg的'''
        result = add(1,2)
        self.assertEqual(4,result,'正常整数加法，没有通过')

    @parameterized.parameterized.expand(
        [
            [1,2,3,'参数化1'],
            [-1,2,1,'参数化第二条'],
            [-1,2,2,'参数化第三条'],
        ]
    )
    def test_param_add(self,a,b,c,desc):
        token = login()
        self._testMethodDoc = desc
        self.desc = desc
        result = add(a,b)
        self.assertEqual(c,result,'预期是%s，实际结果是%s'%(c,result))


if __name__ == '__main__':
    # 不产生测试报告
    # unittest.main()

    ##单个运行某个测试用例
    test_suite = unittest.TestSuite()
    test_suite.addTest(TestAdd('test_add_error2'))
    # test_suite.addTest(TestAdd('test_add_error1'))

    ##运行某个类里面所有的测试用例
    # test_suite = unittest.makeSuite(TestAdd)

    #查找某个目录下的测试用例
    # test_suite = unittest.defaultTestLoader.discover('cases','test*.py')
    #
    with open('report.html','wb') as fw:

        runner = HTMLTestRunner.HTMLTestRunner(stream=fw,title='天马座测试报告',
                                               description='天马座接口测试报告',
                                               verbosity=2
                                               )
    #     runner = HTMLTestRunnerNew.HTMLTestRunner(stream=fw,title='天马座测试报告',
    #                                            description='天马座接口测试报告',
    #                                            verbosity=2
    #                                            )
    #     runner.run(test_suite)
