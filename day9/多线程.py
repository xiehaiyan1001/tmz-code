import threading
import time
import random
def clean():
    print('打扫卫生')
    time.sleep(2)

def xiyifu():
    print('洗衣服')
    time.sleep(3)

def cook():
    print('做饭')
    time.sleep(1)

result_list = []

def export_data(db,excel):
    print(threading.current_thread())
    print('export_data',db,excel)
    time.sleep(random.randint(1,5))
    result_list.append(random.randint(1,5))

#单线程的方式

# clean()
# xiyifu()
# cook()

# start_time = time.time()
# t = threading.Thread(target=clean) #子线程
# t2 = threading.Thread(target=xiyifu)
# t3 = threading.Thread(target=cook)
#
# t.start()
# t2.start()
# t3.start()
#
# t.join()
# t2.join()
# t3.join()
#
# #
# end_time = time.time()
# print(end_time - start_time)

# print(threading.activeCount() ) #当前的线程数


#1、等待多个子线程执行结束，把启动的子线程放到list中，在循环调用t.join
# thread_list = []
#
# for i in range(10):
#     t = threading.Thread(target=export_data)
#     thread_list.append(t)
#     t.start()
#
# for t in thread_list:
#     t.join()
#
# print('数据都导完了')

#2、等待多个子线程执行结束，通过判断当前线程数

for i in range(10):
    t = threading.Thread(target=export_data,args=['db1','a.xls'])
    t.start()

while threading.active_count() != 1:
    pass
print(result_list)
print('数据都导完了')
