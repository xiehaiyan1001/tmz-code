import threading

count = 0

lock = threading.Lock()

def add():
    global count
    print(threading.current_thread(),'开始运行')
    for i in range(1000000):
        lock.acquire()
        count+=1
        lock.release()
        # with lock:
        #     count += 1

for i in range(2):
    t = threading.Thread(target=add)
    t.start()


while threading.active_count()!=1:
    pass

print(count)


