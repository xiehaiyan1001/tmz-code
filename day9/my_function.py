def write_file(file_name,content):
    with open(file_name,'w',encoding='utf-8') as fw:
        fw.write(content)

def add(a,b):
    return a+b

if __name__ == '__main__':
    if add(1,2)==3:
        #正常用例
        print('正常用例，通过')
    else:
        print('正常用例，失败')
    print(add(-1,2))
    print(add(-1,-2))
    print(add(-1.1,-2.0))
    print(add(-1.1,2))
