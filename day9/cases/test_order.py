# -*- coding:utf-8 -*-
# @FileName  :order_test.py
# @Time      :2020-10-18 17:24
# @Author    :niuhanyang
import unittest
def add(a,b):
    return a+b

class TestAdd(unittest.TestCase):
    '''测试order里面的方法'''

    def test_add_normal(self):
        '''正常测试加法的，by huoziyang'''
        result = add(1,2)
        self.assertEqual(3,result)

    def test_add_error1(self):
        '''测试失败使用'''
        result = add(1,2)
        self.assertEqual(4,result)

    def test_add_error2(self):
        '''测试失败有msg的'''
        result = add(1,2)
        self.assertEqual(4,result,'正常整数加法，没有通过')

if __name__ == "__main__":
    pass
