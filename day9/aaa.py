# -*- coding:utf-8 -*-
# @FileName  :aaa.py
# @Time      :2020-10-18 18:05
# @Author    :niuhanyang
import grequests_throttle
import grequests
import time
url = 'http://127.0.0.1:8999/?id=%s'
requests = [ grequests.get(url % i) for i in range(1,10001)]
rate = 100
# req_groups = [ requests[i: i+rate] for i in range(0,len(requests), rate) ]
#
# ret = []
# for req_group in req_groups:
#     ret+=grequests.map(req_group)
#     time.sleep(1)

ret = grequests_throttle.map(requests, rate=rate)

print(ret)
if __name__ == "__main__":
    pass
