import multiprocessing
#lock = multiprocessing.Lock()
import threading
import time

def make_money():
    print('开始挣钱')
    time.sleep(10)

def start_process():
    for i in range(5):
        p = multiprocessing.Process(target=make_money)
        # p = multiprocessing.Process(target=make_money,args=[xxx])
        p.start()
    print(multiprocessing.active_children())#获取当前的进程数
    while len(multiprocessing.active_children()) !=1:
        pass
    print('运行结束')

if __name__ == '__main__':

    start_process()
#

