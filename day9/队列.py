#队列 list
import queue
import random
import time
import threading
orders_q = queue.Queue()

#生产者/消费者模式

def producer():
    for i in range(100):
        order_id = random.randint(1, 99999)
        print('订单生成，orderid:%s'%order_id)
        orders_q.put(order_id)
        time.sleep(1)

def consumer():
    while True:
        if orders_q.qsize()>0:
            oreder_id = orders_q.get()
            print('consumer1,订单落库',oreder_id)


def consumer2():
    while True:
        if orders_q.qsize()>0:
            oreder_id = orders_q.get()
            print('consumer2,订单落库',oreder_id)

t = threading.Thread(target=producer)
t.start()

t = threading.Thread(target=consumer)
t.start()

t = threading.Thread(target=consumer2)
t.start()