#
# pip install -r requriments.txt
# pip freeze > requriments_local.txt

# pip install xxx

import os
import hashlib

user_home = os.environ['USERPROFILE'] if os.environ.get('USERPROFILE') else os.environ.get('HOME')

class InstallRequire:

    local_file = os.path.join(user_home,'.requeriments_cache')#存放本地缓存的文件
    require_file = 'requirements.txt'
    command = 'pip install -r requirements.txt'

    @property
    def check_local_file(self):
        if os.path.exists(self.local_file):
            return True

    @staticmethod
    def read_file(file_name):
        with open(file_name,encoding='utf-8') as fr:
            return fr.read()

    @staticmethod
    def write_file(file_name,content):
        with open(file_name,'w',encoding='utf-8') as fr:
            fr.write(content)

    @staticmethod
    def md5(msg):
        msg = str(msg)
        m = hashlib.md5(msg.encode())
        return m.hexdigest()

    def main(self):
        require_file_md5 = self.md5(self.read_file(self.require_file))
        if self.check_local_file:
            local_file_md5 = self.read_file(self.local_file)
            if local_file_md5 != require_file_md5:
                print('依赖文件有变化，开始安装')
                os.system(self.command)
                self.write_file(self.local_file,require_file_md5)
                print('安装完成')
            else:
                print('依赖文件没有变化继续执行')
        else:
            print('未发现本地缓存文件，开始安装依赖')
            os.system(self.command)
            self.write_file(self.local_file, require_file_md5)

if __name__ == '__main__':
    ir = InstallRequire()
    ir.main()






