import threading
import requests
import auto_install

def down_load_pic(url):
    print('开始下载',url)
    r = requests.get(url)
    file_name = auto_install.InstallRequire.md5(url) + '.jpg'
    with open(file_name,'wb') as fw:
        fw.write(r.content)


def down_load_pics(urls):
    print('开始下载',urls)
    for url in urls:
        r = requests.get(url)
        file_name = auto_install.InstallRequire.md5(url) + '.jpg'
        with open(file_name,'wb') as fw:
            fw.write(r.content)

urls = [
    'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3353166494,2700282750&fm=26&gp=0.jpg',
    'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1603003159808&di=7a97fdd275ec4e0fc4ab54bdb8a2e703&imgtype=0&src=http%3A%2F%2Fwww.pc6.com%2Fup%2F2011-12%2F201112918444441530.jpg',
    'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1603003159808&di=0ae20fe3bd8759c059cb4432938e4062&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20181209%2F38467a58f9264ca68eefa37719b4b739.jpeg',
    'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1603003159807&di=5040439b916279a7106a7660b7e0168a&imgtype=0&src=http%3A%2F%2Fimg1.cache.netease.com%2Fcatchpic%2FE%2FE5%2FE5DD0A8099E2D28226465C6894F7A7A1.jpg'
]

for url in urls:
    t = threading.Thread(target=down_load_pic,args=[url])
    t.start()



# for i in range(10):
#     step = len(urls) / 10
#
#     t = threading.Thread(target=down_load_pics,args=)

while threading.active_count()!=1:
    pass

#50线程，平均分配给每个线程

print('所有图片下载完成')