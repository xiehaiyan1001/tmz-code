import os
import time

day = 60 * 60 * 24 * 3

def str_to_timestamp(str_time,format='%Y-%m-%d'):
    time_tuple = time.strptime(str_time,format)
    return int(time.mktime(time_tuple))


def clean_log(path):
    if os.path.isdir(path):
        for cur_path,dirs,files in os.walk(path):
            for file in files:
                if file.endswith('.log'):

                    #apache_2020-09-04.log
                    #a.log
                    try:
                        file_time = file.split('.')[0].split('_')[-1]
                    except Exception as e:
                        print(e)
                        print('%s 不是标准的日志文件，不处理'%file)
                    else:
                        file_time_stamp = str_to_timestamp(file_time)
                        ago_time_stamp = time.time() - day
                        file_abs_path = os.path.join(cur_path, file)
                        if file_time_stamp < ago_time_stamp or os.path.getsize(file_abs_path) == 0 :
                            if time.strftime('%Y-%m-%d') in file:
                                continue
                            os.remove(file_abs_path)
    else:
        print('路径错误！')


clean_log('logs')


