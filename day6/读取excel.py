import xlrd

book = xlrd.open_workbook('students.xls')

# sheet = book.sheet_by_index(0)
sheet = book.sheet_by_name('sheet1')
print(book.sheets())#所有的sheet页，返回的是一个list，list里面就是每个sheet对象

for s in book.sheets():#
    print(s.row_values(2))


print(sheet.cell(0,0).value)

print(sheet.row_values(0))
print(sheet.row_values(1))

print(sheet.col_values(0))
print(sheet.col_values(1))

print(sheet.nrows)#多少行
print(sheet.ncols)#多少列
