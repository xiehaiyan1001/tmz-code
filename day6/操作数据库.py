import pymysql

#oracle sqlserver ..

host = '118.24.3.40'
user = 'jxz'
password = '123456' #字符串
db='jxz'
port = 3306 #int类型

connect = pymysql.connect(host=host,user=user,
                password=password,
                port=port,db=db,
                autocommit=True
                          )
cur = connect.cursor(pymysql.cursors.DictCursor) #建立游标,仓库管理员
# cur.execute('insert into students values (38,"ljl","nan",19,"天马座","北京");')
# cur.execute('insert into students (name,class) values ("ljj2","天马座");')
# cur.execute('delete from students where id = 21;')
# cur.execute('update students set name="魏强2" where id=20;')

# connect.rollback()#回滚
# connect.commit()#提交
cur.execute('select * from students limit 5;')
print(cur.description) #表的描述
# cur.execute('select * from students where id=1')

# print('fetchmany',cur.fetchmany(2))
# print('fetchone',cur.fetchone())
print('fetchall',cur.fetchall())#拿到所有的结果

# for data in cur:
#     print(data)

cur.close()
connect.close()

