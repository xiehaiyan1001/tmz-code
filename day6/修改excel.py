from xlutils import copy
import xlrd

book = xlrd.open_workbook('students.xls')
sheet = book.sheet_by_index(0)

new_book = copy.copy(book)
copy_sheet = new_book.get_sheet(0)
                 #1,7     #1 2 3 4 5 6
for row in range(1,sheet.nrows-1):#1 2 3 4 5 6 7
    addr = sheet.cell(row,2).value
    addr = addr.replace('beijing',"北京").replace('shanghai','上海')
    copy_sheet.write(row,2,addr)


new_book.save('students.xls')
