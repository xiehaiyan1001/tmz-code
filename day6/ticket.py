# import random
#
# number = input('number:').strip()
# if number.isdigit() and int(number)>0:
#     l = []
#     while True:
#         red = random.sample(range(1,34),6)
#         red.sort()
#         blue = random.sample(range(1,18),1)
#         result = red + blue
#         result = [ str(ball).zfill(2) for ball in result]
#         seq = ' '.join(result)
#         if seq not in l:
#             l.append(seq)
#             print('生成的双色球号码是：红球：%s 蓝球是:%s'%(' '.join(result[:6]),result[-1]))
#         if int(number) == len(l):
#             break




import random

reds = [ str(ball).zfill(2) for ball in range(1,34)]
blues = [ str(ball).zfill(2) for ball in range(1,18)]


def seq():
    number = input('number:').strip()
    if number.isdigit() and int(number)>0:
        l = set()
        while int(number)!=len(l):
            red = random.sample(reds,6)
            red.sort()
            blue = random.sample(blues,1)
            result = red + blue
            seq = ' '.join(result)+'\n'
            l.add(seq)
            print('生成的双色球号码是：红球：%s 蓝球是:%s'%(' '.join(result[:6]),result[-1]))

    with open('seq.txt','w',encoding='utf-8') as fw:
        fw.writelines(l)


seq()